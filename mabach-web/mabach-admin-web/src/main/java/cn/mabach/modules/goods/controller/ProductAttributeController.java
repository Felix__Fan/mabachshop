package cn.mabach.modules.goods.controller;

import cn.mabach.modules.goods.feign.ProductAttributeServiceFeign;
import cn.mabach.goods.dto.ProductAttrInfo;
import cn.mabach.goods.entity.ProductAttributeEntity;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import io.swagger.annotations.ApiOperation;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 商品属性参数表
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-26 13:46:12
 */
@RestController
@RequestMapping("/productAttribute")
@Slf4j
public class ProductAttributeController {
    @Autowired
    private ProductAttributeServiceFeign productAttributeService;

    /**
     * 列表
     */


    @GetMapping(value = "/list/{cid}")
    public PageResult<ProductAttributeEntity> getList(@PathVariable Long cid,
                                                      @RequestParam(value = "type",required = false) Integer type,
                                                      @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                      @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {
        RS<PageResult<ProductAttributeEntity>> pageResultRS = productAttributeService.queryPage(cid,type,pageNum, pageSize);
        log.info("ProductAttributeController:{},id:{}",Thread.currentThread().getName(),Thread.currentThread().getId());
        return pageResultRS.getData();
    }



    /**
     * 信息
     */

    @GetMapping(value = "/{id}")
    public ProductAttributeEntity getItem(@PathVariable("id") Long id) {
        return productAttributeService.getByIdE(id).getData();
    }





    /**
     * 保存
     */
    @PostMapping(value = "/create")
    public RS create(@Validated @RequestBody ProductAttributeEntity productAttributeEntity, BindingResult result) {

        RS rs = productAttributeService.saveE(productAttributeEntity);

        return rs;
    }



    /**
     * 修改
     */
//    @PostMapping(value = "/update/{id}")
//    @PreAuthorize("@ss.hasPermi('pms:productAttribute:update')")
//    public RS update(@PathVariable("id") Long id,
//                      @RequestBody ProductAttributeParam entity) {
//
//        entity.setId(id);
//        ProductAttributeEntity productAttributeEntity = BeanUtilsMabach.doToDto(entity, ProductAttributeEntity.class);
//        RS rs = productAttributeService.updateByIdE(productAttributeEntity);
//
//        return rs;
//    }






    /**
     * 批量删除
     */
    @PostMapping(value = "/delete")
    public RS deleteBatch(@RequestParam("ids") List<Long> ids) {
        RS rs = productAttributeService.removeByIdsE(ids);
        return rs;
    }


    @ApiOperation("根据商品分类的id获取商品属性及属性分类")
    @RequestMapping(value = "/attrInfo/{productCategoryId}", method = RequestMethod.GET)
    public List<ProductAttrInfo> getAttrInfo(@PathVariable Long productCategoryId) {

        return productAttributeService.getAttrInfo(productCategoryId).getData();
    }


}
