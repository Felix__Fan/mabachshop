package cn.mabach.modules.business.feign;

import cn.mabach.business.service.CouponProductCategoryRelationService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "app-mabach-business")
public interface CouponProductCategoryRelationServiceFeign extends CouponProductCategoryRelationService {
}
