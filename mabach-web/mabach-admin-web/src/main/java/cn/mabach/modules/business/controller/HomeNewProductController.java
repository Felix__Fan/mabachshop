package cn.mabach.modules.business.controller;

import cn.mabach.business.entity.HomeNewProductEntity;
import cn.mabach.modules.business.feign.HomeNewProductServiceFeign;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 新鲜好物表
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-31 21:38:07
 */
@RestController
@RequestMapping("/home/newProduct")
public class HomeNewProductController {
    @Autowired
    private HomeNewProductServiceFeign homeNewProductService;



    /**
        * 分页列表
        */
    @GetMapping(value = "/list")
    public PageResult<HomeNewProductEntity> getList(@RequestParam(value = "productName", required = false) String productName,
                                                    @RequestParam(value = "recommendStatus", required = false) Integer recommendStatus,
                                                    @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                    @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {

        RS<PageResult<HomeNewProductEntity>> pageResultRS = homeNewProductService.queryPage(productName,recommendStatus, pageNum, pageSize);

        return pageResultRS.getData();
    }



    /**
     * 根据ID获取信息
     */

    @GetMapping(value = "/{id}")
    public HomeNewProductEntity getItem(@PathVariable("id") Long id) {
        return homeNewProductService.getByIdE(id).getData();
    }





    /**
     * 根据id保存
     */
    @Transactional
    @PostMapping(value = "/create")
    public RS create(@RequestBody List<HomeNewProductEntity> selectProducts) {
        for (HomeNewProductEntity homeNewProductEntity : selectProducts) {
            RS rs = homeNewProductService.saveE(homeNewProductEntity);
            if (rs.getCode()!=200){
                return RS.error();
            }
        }


        return RS.ok();
    }



    /**
     * 根据id修改
     */
    @PostMapping(value = "/update/{id}")
    public RS update(@PathVariable("id") Long id,
                     @Validated @RequestBody HomeNewProductEntity entity,
                     BindingResult result) {
        entity.setId(id);
        RS rs = homeNewProductService.updateByIdE(entity);

        return rs;
    }

    /**
       * 根据id集合删除
       */
    @PostMapping(value = "/delete")
    public RS deleteBatch(@RequestParam("ids") List<Long> ids) {
        RS rs = homeNewProductService.removeByIdsE(ids);
        return rs;
    }

    /**
         * 根据id删除
         */
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
    public RS removeByIdE(@PathVariable Long id) {

        return homeNewProductService.removeByIdE(id);

    }


/*
* 修改推荐排序
* */
    @RequestMapping(value = "/update/sort", method = RequestMethod.POST)
    public RS updateSort(@RequestParam("id") Long id, @RequestParam("sort") Integer sort) {
        return homeNewProductService.updateSort(id,sort);
    }
    /*
     * 批量修改推荐状态
     * */
    @RequestMapping(value = "/update/recommendStatus", method = RequestMethod.POST)
    public RS updateRecommendStatus(@RequestParam("ids") List<Long> ids, @RequestParam Integer recommendStatus) {
        return homeNewProductService.updateRecommendStatus(ids,recommendStatus);
    }
}
