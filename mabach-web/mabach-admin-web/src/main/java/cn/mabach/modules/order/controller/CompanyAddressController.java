package cn.mabach.modules.order.controller;


import cn.mabach.order.entity.CompanyAddressEntity;
import cn.mabach.order.service.CompanyAddressService;


import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 公司收发货地址表
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2020-01-02 23:46:11
 */
@RestController
@RequestMapping("/companyAddress")
public class CompanyAddressController {
    @Autowired
    private CompanyAddressService companyAddressService;



    /**
        * 分页列表
        */
    @GetMapping(value = "/list")
    public PageResult<CompanyAddressEntity> getList(@RequestParam(value = "keyword", required = false) String keyword,
                                                    @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                    @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {

        RS<PageResult<CompanyAddressEntity>> pageResultRS = companyAddressService.queryPage(keyword, 1, 100);

        return pageResultRS.getData();
    }



    /**
     * 根据ID获取信息
     */

    @GetMapping(value = "/{id}")
    public CompanyAddressEntity getItem(@PathVariable("id") Long id) {
        return companyAddressService.getByIdE(id).getData();
    }





    /**
     * 根据id保存
     */
    @PostMapping(value = "/create")
    public RS create(@RequestBody CompanyAddressEntity companyAddressEntity) {

        RS rs = companyAddressService.saveE(companyAddressEntity);

        return rs;
    }



    /**
     * 根据id修改
     */
    @PostMapping(value = "/update/{id}")
    public RS update(@PathVariable("id") Long id,
                     @Validated @RequestBody CompanyAddressEntity entity,
                     BindingResult result) {
        entity.setId(id);
        RS rs = companyAddressService.updateByIdE(entity);

        return rs;
    }

    /**
       * 根据id集合删除
       */
    @PostMapping(value = "/delete")
    public RS deleteBatch(@RequestParam("ids") List<Long> ids) {
        RS rs = companyAddressService.removeByIdsE(ids);
        return rs;
    }

    /**
         * 根据id删除
         */
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
    public RS removeByIdE(@PathVariable Long id) {

        return companyAddressService.removeByIdE(id);

    }


}
