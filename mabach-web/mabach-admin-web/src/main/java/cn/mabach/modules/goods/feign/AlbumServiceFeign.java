package cn.mabach.modules.goods.feign;

import cn.mabach.goods.service.AlbumService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "app-mabach-goods")
public interface AlbumServiceFeign extends AlbumService {
}
