package cn.mabach.modules.business.feign;

import cn.mabach.business.service.HomeBrandService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "app-mabach-business")
public interface HomeBrandServiceFeign extends HomeBrandService {
}
