package cn.mabach.modules.business.controller;

import cn.mabach.business.dto.CouponParam;
import cn.mabach.business.entity.CouponEntity;
import cn.mabach.modules.business.feign.CouponServiceFeign;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 优惠卷表
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-31 18:31:56
 */
@RestController
@RequestMapping("/coupon")
public class CouponController {


@Autowired
private CouponServiceFeign couponService;


    /**
        * 分页列表
        */
    @GetMapping(value = "/list")
    public PageResult<CouponEntity> getList(@RequestParam(value = "name",required = false) String name,
                                            @RequestParam(value = "type",required = false) Integer type,
                                            @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                            @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {

        RS<PageResult<CouponEntity>> pageResultRS = couponService.queryPage(name,type, pageNum, pageSize);

        return pageResultRS.getData();
    }



    /**
     * 根据ID获取信息
     */

    @GetMapping(value = "/{id}")
    public CouponParam getItem(@PathVariable("id") Long id) {
        return couponService.getByIdE(id).getData();
    }





    /**
     * 根据id保存
     */
    @PostMapping(value = "/create")
    public RS create(@RequestBody CouponParam couponParam) {

        RS rs = couponService.saveE(couponParam);

        return rs;
    }

    /**
     * 删除优惠券
     */

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
    public RS removeByIdE(@PathVariable Long id) {

        return couponService.removeByIdE(id);

    }


    /**
     * 根据id修改
     */
    @PostMapping(value = "/update/{id}")
    public RS update(@PathVariable("id") Long id,
                     @Validated @RequestBody CouponParam entity,
                     BindingResult result) {
        entity.setId(id);
        RS rs = couponService.updateByIdE(entity);

        return rs;
    }

    /**
       * 根据id集合删除
       */
    @PostMapping(value = "/delete")
    public RS deleteBatch(@RequestParam("ids") List<Long> ids) {
        RS rs = couponService.removeByIdsE(ids);
        return rs;
    }





}
