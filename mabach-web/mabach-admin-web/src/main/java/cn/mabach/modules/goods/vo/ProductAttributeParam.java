package cn.mabach.modules.goods.vo;

import lombok.Data;

@Data
public class ProductAttributeParam {
    Long id;
    String name;
}
