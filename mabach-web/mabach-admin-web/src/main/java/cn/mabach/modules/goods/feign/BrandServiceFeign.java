package cn.mabach.modules.goods.feign;

import cn.mabach.goods.service.BrandService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "app-mabach-goods")
public interface BrandServiceFeign extends BrandService {
}
