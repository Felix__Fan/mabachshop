package cn.mabach.modules.business.feign;

import cn.mabach.business.service.FlashPromotionSessionService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "app-mabach-business")
public interface FlashPromotionSessionServiceFeign extends FlashPromotionSessionService {
}
