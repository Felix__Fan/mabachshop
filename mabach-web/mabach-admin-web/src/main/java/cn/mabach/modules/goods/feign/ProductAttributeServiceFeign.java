package cn.mabach.modules.goods.feign;

import cn.mabach.goods.service.ProductAttributeService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "app-mabach-goods")
public interface ProductAttributeServiceFeign extends ProductAttributeService {
}
