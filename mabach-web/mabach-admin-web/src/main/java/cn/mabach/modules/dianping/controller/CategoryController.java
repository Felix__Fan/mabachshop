package cn.mabach.modules.dianping.controller;

import java.util.Arrays;
import java.util.Map;

import java.util.Arrays;
import java.util.Date;

import cn.mabach.dianping.entity.CategoryEntity;
import cn.mabach.modules.dianping.feign.CategoryServiceFeign;
import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;



/**
 * 
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-02-28 18:02:46
 */
@RestController
@RequestMapping("/category")
public class CategoryController {
    @Autowired
    private CategoryServiceFeign categoryService;



    /**
     * 列表
     */
    @RequestMapping("/list")
    public PageResult list(@RequestParam(value = "keyword", required = false) String keyword,
                  @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                  @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize){
        PageResult page = categoryService.queryPage(keyword,pageNum,pageSize);

        return page;
    }


    /**
     * 信息
     */

    @RequestMapping("/{id}")
    public CategoryEntity info(@PathVariable("id") Integer id){
            CategoryEntity seller = categoryService.getByIdE(id);

        return seller;
    }



    /**
     * 保存
     */
    @RequestMapping("/save")
    public RS save(@RequestBody CategoryEntity entity){
        entity.setCreatedTime(new Date());
        entity.setUpdatedTime(new Date());

            categoryService.saveE(entity);

        return RS.ok();
    }


    /**
     * 修改
     */

    @RequestMapping("/update/{id}")
    public RS update(@PathVariable("id") Integer id,@RequestBody CategoryEntity entity){
        entity.setUpdatedTime(new Date());
            categoryService.updateByIdE(entity);

        return RS.ok();
    }

    /**
     * 删除
     */

    @RequestMapping("/delete")
    public RS delete(@RequestBody Integer[] ids){
            categoryService.removeByIdsE(Arrays.asList(ids));
        return RS.ok();
    }


    /**
  * 根据id删除
  */
    @RequestMapping("/delete/{id}")
    public RS delete(@PathVariable("id") Integer id){
            categoryService.removeOneById(id);

        return RS.ok();
    }

}
