package cn.mabach.modules.order.controller;


import cn.mabach.order.entity.OrderReturnApplyEntity;
import cn.mabach.order.service.OrderReturnApplyService;
import cn.mabach.order.dto.OrderReturnApplyResult;
import cn.mabach.order.vo.UpdateStatusParam;


import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;

import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;


/**
 * 订单退货申请
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2020-01-02 23:46:11
 */
@RestController
@RequestMapping("/returnApply")
public class OrderReturnApplyController {
    @Autowired
    private OrderReturnApplyService orderReturnApplyService;



    /**
        * 分页列表
        */
    @GetMapping(value = "/list")
    public PageResult<OrderReturnApplyEntity> getList(@RequestParam(value = "id",required = false) Long id,
                                                      @RequestParam(value = "receiverKeyword",required = false) String receiverKeyword,
                                                      @RequestParam(value = "status",required = false) Integer status,
                                                      @RequestParam(value = "createTime",required = false)
                                                              @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date createTime,
                                                      @RequestParam(value = "handleMan",required = false) String handleMan,
                                                      @RequestParam(value = "handleTime",required = false)
                                                              @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date handleTime,
                                                      @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                      @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {

        RS<PageResult<OrderReturnApplyEntity>> pageResultRS = orderReturnApplyService.queryPage(id,receiverKeyword, status,
                createTime,handleMan,handleTime,pageNum, pageSize);

        return pageResultRS.getData();
    }



    /**
     * 根据ID获取信息
     */

    @GetMapping(value = "/{id}")
    public OrderReturnApplyResult getItem(@PathVariable("id") Long id) {
        return orderReturnApplyService.getByIdE(id).getData();
    }





    /**
     * 根据id保存
     */
    @PostMapping(value = "/create")
    public RS create(@RequestBody OrderReturnApplyEntity orderReturnApplyEntity) {

        RS rs = orderReturnApplyService.saveE(orderReturnApplyEntity);

        return rs;
    }



    /**
     * 根据id修改
     */
    @PostMapping(value = "/update/{id}")
    public RS update(@PathVariable("id") Long id,
                     @Validated @RequestBody OrderReturnApplyEntity entity,
                     BindingResult result) {
        entity.setId(id);
        RS rs = orderReturnApplyService.updateByIdE(entity);

        return rs;
    }

    /**
       * 根据id集合删除
       */
    @PostMapping(value = "/delete")
    public RS deleteBatch(@RequestParam("ids") List<Long> ids) {
        RS rs = orderReturnApplyService.removeByIdsE(ids);
        return rs;
    }

    /**
         * 根据id删除
         */
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
    public RS removeByIdE(@PathVariable Long id) {

        return orderReturnApplyService.removeByIdE(id);

    }

    /*
    * 修改申请状态
    * */
    @RequestMapping(value = "/update/status", method = RequestMethod.POST)
    public RS updateStatus(@RequestBody UpdateStatusParam statusParam) {
        return orderReturnApplyService.updateStatus(statusParam);
    }


}
