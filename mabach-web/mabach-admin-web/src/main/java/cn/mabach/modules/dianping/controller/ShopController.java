package cn.mabach.modules.dianping.controller;

import java.util.Arrays;
import java.util.Map;

import java.util.Arrays;
import java.util.Date;

import cn.mabach.dianping.entity.ShopEntity;
import cn.mabach.modules.dianping.feign.ShopServiceFeign;
import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;


/**
 * 
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-02-28 20:06:25
 */
@RestController
@RequestMapping("/store")
@Slf4j
public class ShopController {
    @Autowired
    private ShopServiceFeign shopService;
    @Autowired
    private RestTemplate restTemplate;
    private String baiduApi="http://api.map.baidu.com/geocoding/v3/?output=json&ak=Fs6Dw2zAeCVBG40Yy7DCEZmA7uQqqBUY&callback=showLocation&address=%s";



    @RequestMapping("/getAdsress")
    public Map getAdsress(@RequestParam("address")String address){
        String s = String.format(baiduApi, address);
        String res = restTemplate.getForObject(s, String.class);
        String replace = res.replace("showLocation&&showLocation(", "").replace(")", "");
        log.info("replace:{}",replace);
        Map map = JSON.parseObject(replace, Map.class);
        log.info("Map:{}",map);

        return map;
    }
    /**
     * 列表
     */
    @RequestMapping("/list")
    public PageResult list(@RequestParam(value = "keyword", required = false) String keyword,
                  @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                  @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize){
        PageResult page = shopService.queryPage(keyword,pageNum,pageSize);

        return page;
    }


    /**
     * 信息
     */

    @RequestMapping("/{id}")
    public ShopEntity info(@PathVariable("id") Integer id){
            ShopEntity seller = shopService.getByIdE(id);

        return seller;
    }



    /**
     * 保存
     */
    @RequestMapping("/save")
    public RS save(@RequestBody ShopEntity entity){

            shopService.saveE(entity);

        return RS.ok();
    }


    /**
     * 修改
     */

    @RequestMapping("/update/{id}")
    public RS update(@PathVariable("id") Integer id,@RequestBody ShopEntity entity){

            shopService.updateByIdE(entity);

        return RS.ok();
    }

    /**
     * 删除
     */

    @RequestMapping("/delete")
    public RS delete(@RequestBody Integer[] ids){
            shopService.removeByIdsE(Arrays.asList(ids));
        return RS.ok();
    }


    /**
  * 根据id删除
  */
    @RequestMapping("/delete/{id}")
    public RS delete(@PathVariable("id") Integer id){
            shopService.removeOneById(id);

        return RS.ok();
    }

}
