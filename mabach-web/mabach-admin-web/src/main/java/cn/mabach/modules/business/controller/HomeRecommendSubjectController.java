package cn.mabach.modules.business.controller;

import cn.mabach.business.entity.HomeRecommendSubjectEntity;
import cn.mabach.modules.business.feign.HomeRecommendSubjectServiceFeign;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 首页推荐专题表
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-31 21:38:43
 */
@RestController
@RequestMapping("/home/recommendSubject")
public class HomeRecommendSubjectController {
    @Autowired
    private HomeRecommendSubjectServiceFeign homeRecommendSubjectService;



    /**
        * 分页列表
        */
    @GetMapping(value = "/list")
    public PageResult<HomeRecommendSubjectEntity> getList(@RequestParam(value = "subjectName", required = false) String subjectName,
                                                          @RequestParam(value = "recommendStatus", required = false) Integer recommendStatus,
                                                          @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                          @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {

        RS<PageResult<HomeRecommendSubjectEntity>> pageResultRS = homeRecommendSubjectService.queryPage(subjectName,recommendStatus, pageNum, pageSize);

        return pageResultRS.getData();
    }



    /**
     * 根据ID获取信息
     */

    @GetMapping(value = "/{id}")
    public HomeRecommendSubjectEntity getItem(@PathVariable("id") Long id) {
        return homeRecommendSubjectService.getByIdE(id).getData();
    }





    /**
     * 根据id保存
     */
    @Transactional
    @PostMapping(value = "/create")
    public RS create(@RequestBody List<HomeRecommendSubjectEntity> selectSubjects) {
        for (HomeRecommendSubjectEntity homeRecommendSubjectEntity : selectSubjects) {
            RS rs = homeRecommendSubjectService.saveE(homeRecommendSubjectEntity);
            if (rs.getCode()!=200){
                return RS.error();
            }
        }


        return RS.ok();
    }



    /**
     * 根据id修改
     */
    @PostMapping(value = "/update/{id}")
    public RS update(@PathVariable("id") Long id,
                     @Validated @RequestBody HomeRecommendSubjectEntity entity,
                     BindingResult result) {
        entity.setId(id);
        RS rs = homeRecommendSubjectService.updateByIdE(entity);

        return rs;
    }

    /**
       * 根据id集合删除
       */
    @PostMapping(value = "/delete")
    public RS deleteBatch(@RequestParam("ids") List<Long> ids) {
        RS rs = homeRecommendSubjectService.removeByIdsE(ids);
        return rs;
    }

    /**
         * 根据id删除
         */
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
    public RS removeByIdE(@PathVariable Long id) {

        return homeRecommendSubjectService.removeByIdE(id);

    }
/*
* 修改推荐排序
* */
    @RequestMapping(value = "/update/sort", method = RequestMethod.POST)
    public RS updateSort(@RequestParam("id") Long id, @RequestParam("sort") Integer sort) {
       return homeRecommendSubjectService.updateSort(id,sort);
    }

    /*
     * 批量修改推荐状态
     * */
    @RequestMapping(value = "/update/recommendStatus", method = RequestMethod.POST)
    public RS updateRecommendStatus(@RequestParam("ids") List<Long> ids, @RequestParam Integer recommendStatus) {
        return homeRecommendSubjectService.updateRecommendStatus(ids,recommendStatus);
    }
}
