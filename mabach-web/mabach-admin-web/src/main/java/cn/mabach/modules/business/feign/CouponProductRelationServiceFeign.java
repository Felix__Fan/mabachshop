package cn.mabach.modules.business.feign;

import cn.mabach.business.service.CouponProductRelationService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "app-mabach-business")
public interface CouponProductRelationServiceFeign extends CouponProductRelationService {
}
