package cn.mabach.modules.business.controller;

import cn.mabach.business.entity.FlashPromotionEntity;
import cn.mabach.modules.business.feign.FlashPromotionServiceFeign;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.List;


/**
 * 限时购表
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-31 21:38:07
 */
@RestController
@RequestMapping("/flash")
public class FlashPromotionController {
    @Autowired
    private FlashPromotionServiceFeign flashPromotionService;



    /**
        * 分页列表
        */
    @GetMapping(value = "/list")
    public PageResult<FlashPromotionEntity> getList(@RequestParam(value = "keyword", required = false) String keyword,
                                                    @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                    @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
                                                        ) {

        RS<PageResult<FlashPromotionEntity>> pageResultRS = flashPromotionService.queryPage(keyword, pageNum, pageSize);

        return pageResultRS.getData();
    }



    /**
     * 根据ID获取信息
     */

    @GetMapping(value = "/{id}")
    public FlashPromotionEntity getItem(@PathVariable("id") Long id) {
        return flashPromotionService.getByIdE(id).getData();
    }





    /**
     * 根据id保存
     */
    @PostMapping(value = "/create")
    public RS create(@RequestBody FlashPromotionEntity flashPromotionEntity) throws ParseException {
        System.out.println(flashPromotionEntity.getStartDate().toString());

        RS rs = flashPromotionService.saveE(flashPromotionEntity);

        return rs;
    }



    /**
     * 根据id修改
     */
    @PostMapping(value = "/update/{id}")
    public RS update(@PathVariable("id") Long id,
                     @Validated @RequestBody FlashPromotionEntity entity,
                     BindingResult result) {
        entity.setId(id);
        RS rs = flashPromotionService.updateByIdE(entity);

        return rs;
    }

    /**
       * 根据id集合删除
       */
    @PostMapping(value = "/delete")
    public RS deleteBatch(@RequestParam("ids") List<Long> ids) {
        RS rs = flashPromotionService.removeByIdsE(ids);
        return rs;
    }

    /**
         * 根据id删除
         */
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
    public RS removeByIdE(@PathVariable Long id) {

        return flashPromotionService.removeByIdE(id);

    }
    /**
     * 修改上线状态
     */


    @RequestMapping(value = "/update/status/{id}", method = RequestMethod.POST)
    public RS update(@PathVariable Long id, Integer status) {
        return flashPromotionService.update(id,status);
    }

}
