package cn.mabach.modules.business.controller;

import cn.mabach.business.entity.FlashPromotionSessionEntity;
import cn.mabach.business.dto.FlashPromotionSessionDetail;
import cn.mabach.modules.business.feign.FlashPromotionSessionServiceFeign;


import cn.mabach.result.RS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 限时购场次表
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-31 21:38:07
 */
@RestController
@RequestMapping("/flashSession")
public class FlashPromotionSessionController {
    @Autowired
    private FlashPromotionSessionServiceFeign flashPromotionSessionService;




    /**
        * 分页列表
        */
//    @GetMapping(value = "/list")
//    @PreAuthorize("@ss.hasPermi('sms:flashSession:read')")
//    public RS<PageResult<FlashPromotionSessionEntity>> getList(@RequestParam(value = "keyword", required = false) String keyword,
//                                                               @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
//                                                               @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {
//
//        RS<PageResult<FlashPromotionSessionEntity>> pageResultRS = flashPromotionSessionService.queryPage(keyword, pageNum, pageSize);
//
//        return pageResultRS;
//    }



    /**
     * 根据ID获取信息
     */

    @GetMapping(value = "/{id}")
    public FlashPromotionSessionEntity getItem(@PathVariable("id") Long id) {
        return flashPromotionSessionService.getByIdE(id).getData();
    }





    /**
     * 根据id保存
     */
    @PostMapping(value = "/create")
    public RS create(@RequestBody FlashPromotionSessionEntity flashPromotionSessionEntity) {

        RS rs = flashPromotionSessionService.saveE(flashPromotionSessionEntity);

        return rs;
    }



    /**
     * 根据id修改
     */
    @PostMapping(value = "/update/{id}")
    public RS update(@PathVariable("id") Long id,
                     @Validated @RequestBody FlashPromotionSessionEntity entity,
                     BindingResult result) {
        entity.setId(id);
        RS rs = flashPromotionSessionService.updateByIdE(entity);

        return rs;
    }

    /**
       * 根据id集合删除
       */
    @PostMapping(value = "/delete")
    public RS deleteBatch(@RequestParam("ids") List<Long> ids) {
        RS rs = flashPromotionSessionService.removeByIdsE(ids);
        return rs;
    }

    /**
         * 根据id删除
         */
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
    public RS removeByIdE(@PathVariable Long id) {

        return flashPromotionSessionService.removeByIdE(id);

    }


    @RequestMapping(value = "/update/status/{id}", method = RequestMethod.POST)
    public RS updateStatus(@PathVariable Long id, @RequestParam("status") Integer status) {
       return flashPromotionSessionService.updateStatus(id,status);
    }


    /**
     * 获取全部场次
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public List<FlashPromotionSessionEntity> listAll() {
       return flashPromotionSessionService.listAll().getData();
    }

    /**
     * 获取全部可选场次及其数量
     */
    @RequestMapping(value = "/selectList", method = RequestMethod.GET)
    public List<FlashPromotionSessionDetail> selectList(@RequestParam("flashPromotionId") Long flashPromotionId) {
       return flashPromotionSessionService.selectList(flashPromotionId).getData();
    }
}

