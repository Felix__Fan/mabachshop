package cn.mabach.modules.goods.controller;

import cn.mabach.goods.entity.ProductCategoryEntity;
import cn.mabach.modules.goods.feign.ProductCategoryServiceFeign;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


/**
 * 产品分类
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-26 13:45:39
 */
@RestController
@RequestMapping("/productCategory")
@Slf4j
public class ProductCategoryController {
    @Autowired
    private ProductCategoryServiceFeign productCategoryService;

    /**
     * 列表
     */



    @GetMapping(value = "/list/{parentId}")
    public PageResult<ProductCategoryEntity> getList(@PathVariable Long parentId,
                                                         @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                         @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {
        RS<PageResult<ProductCategoryEntity>> pageResultRS = productCategoryService.queryPage(parentId, pageNum, pageSize);
        log.info("ProductCategoryController:{},id:{}",Thread.currentThread().getName(),Thread.currentThread().getId());
        return pageResultRS.getData();
    }



    /**
     * 信息
     */

    @GetMapping(value = "/{id}")
    public ProductCategoryEntity getItem(@PathVariable("id") Long id) {
        return productCategoryService.getByIdE(id).getData();
    }





    /**
     * 保存
     */
    @PostMapping(value = "/create")
    public RS create(@Validated @RequestBody ProductCategoryEntity productCategoryEntity, BindingResult result) {

        RS rs = productCategoryService.saveE(productCategoryEntity);

        return rs;
    }






    /**
     * 修改
     */
    @PostMapping(value = "/update/{id}")
    public RS update(@PathVariable("id") Long id,
                     @Validated @RequestBody ProductCategoryEntity entity,
                     BindingResult result) {
        entity.setId(id);
        RS rs = productCategoryService.updateByIdE(entity);

        return rs;
    }




    /**
     * 删除
     */
    @GetMapping(value = "/delete/{id}")
    public RS delete(@PathVariable("id") Long id) {
        RS rs = productCategoryService.removeOneById(id);

        return rs;

    }


    @PostMapping(value = "/delete/batch")
    public RS deleteBatch(@RequestParam("ids") List<Long> ids) {
        RS rs = productCategoryService.removeByIdsE(ids);
        return rs;
    }



    @PostMapping(value = "/update/navStatus")
    public RS updateNavStatus(@RequestParam("ids") Long ids,@RequestParam("navStatus") Integer navStatus) {
         return productCategoryService.updateNavStatus(ids,navStatus);

    }



    @PostMapping(value = "/update/showStatus")
    public RS updateShowStatus(@RequestParam("ids") Long ids,@RequestParam("showStatus") Integer showStatus) {

        return productCategoryService.updateShowStatus(ids,showStatus);

    }

    @RequestMapping(value = "/list/withChildren", method = RequestMethod.GET)
    public List<Map> listWithChildren() {

        return productCategoryService.listWithChildren().getData();
    }
}
