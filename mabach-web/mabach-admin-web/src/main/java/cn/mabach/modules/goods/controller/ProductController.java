package cn.mabach.modules.goods.controller;

import cn.mabach.modules.goods.feign.ProductServiceFeign;
import cn.mabach.modules.goods.feign.ProductVertifyRecordServiceFeign;
import cn.mabach.modules.sys.entity.SysUserEntity;
import cn.mabach.modules.sys.entity.SysUserTokenEntity;
import cn.mabach.modules.sys.service.ShiroService;
import cn.mabach.goods.dto.ProductParam;
import cn.mabach.goods.dto.ProductResult;
import cn.mabach.goods.entity.ProductEntity;
import cn.mabach.goods.entity.ProductVertifyRecordEntity;
import cn.mabach.goods.vo.ProductQueryParam;


import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.List;


/**
 * 商品信息
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-26 13:45:39
 */
@RestController
@RequestMapping("/product")
@Slf4j
public class ProductController {
    @Autowired
    private ProductServiceFeign productService;
    @Autowired
    private ProductVertifyRecordServiceFeign productVertifyRecordServiceFeign;
    @Autowired
    private ShiroService shiroService;



    /**
     * 创建商品
     */
    @PostMapping(value = "/create")
    public RS create(@RequestBody ProductParam productParam) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {

        return productService.saveE(productParam);


    }


    /**
     * 信息
     */

    @GetMapping(value = "/updateInfo/{id}")
    public ProductResult getItem(@PathVariable("id") Long id) {
        return productService.getByIdE(id).getData();
    }




    /**
     * 修改
     */
    @PostMapping(value = "/update/{id}")
    public RS update(@PathVariable("id") Long id, @RequestBody ProductParam entity) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        entity.setId(id);
        RS rs = productService.updateByIdE(entity);

        return rs;
    }


    /**
     * 分页列表
     */

    @RequestMapping(value = "/list")
    public PageResult<ProductEntity> getList(ProductQueryParam productQueryParam,
                                             @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                             @RequestParam(value = "pageSize", defaultValue = "5") Integer pageSize) {
        RS<PageResult<ProductEntity>> pageResultRS = productService.queryPage(productQueryParam, pageNum, pageSize);

        log.info("ProductController:{},id:{}",Thread.currentThread().getName(),Thread.currentThread().getId());
        ThreadLocal<String> threadLocal = new ThreadLocal<>();
        log.info("threadLocal：{}",threadLocal.get());
        return pageResultRS.getData();
    }





   //"批量修改审核状态")
    @PostMapping(value = "/update/verifyStatus")
    @Transactional
    public RS updateVerifyStatus(@RequestParam("ids") List<Long> ids,
                                 @RequestParam("verifyStatus") Integer verifyStatus,
                                 @RequestParam("detail") String detail, HttpServletRequest request) {
        Boolean flag=false;

        for (Long id : ids) {
            productService.updateVerifyStatus(id,verifyStatus);
            ProductVertifyRecordEntity recordEntity = new ProductVertifyRecordEntity();
            recordEntity.setProductId(id);
            recordEntity.setCreateTime(new Date());
            recordEntity.setStatus(verifyStatus);
            recordEntity.setDetail(detail);
//            获取管理员用户名
            String token = request.getHeader("token");
            SysUserTokenEntity sysUserTokenEntity = shiroService.queryByToken(token);
            SysUserEntity sysUserEntity = shiroService.queryUser(sysUserTokenEntity.getUserId());
            recordEntity.setVertifyMan(sysUserEntity.getUsername());
            productVertifyRecordServiceFeign.saveE(recordEntity);

        }

        flag=true;
        if (flag){
            return RS.ok();
        }

       return RS.error();
    }

    /**
     * 批量上下架
     */
    @PostMapping(value = "/update/publishStatus")
    public RS updatePublishStatus(@RequestParam("ids") List<Long> ids,
                                  @RequestParam("publishStatus") Integer publishStatus) {
       return productService.updatePublishStatus(ids,publishStatus);
    }


    /**
     * 批量推荐商品
     */
    @PostMapping(value = "/update/recommendStatus")
    public RS updateRecommendStatus(@RequestParam("ids") List<Long> ids,
                                              @RequestParam("recommendStatus") Integer recommendStatus) {
       return productService.updateRecommendStatus(ids,recommendStatus);
    }


    /**
     * 批量设为新品
     */

    @PostMapping(value = "/update/newStatus")
    public RS updateNewStatus(@RequestParam("ids") List<Long> ids,
                              @RequestParam("newStatus") Integer newStatus) {
        return productService.updateNewStatus(ids,newStatus);

    }

    /**
     * 批量逻辑删除
     */
    @PostMapping(value = "/update/deleteStatus")
    public RS updateDeleteStatus(@RequestParam("ids") List<Long> ids,
                                 @RequestParam("deleteStatus") Integer deleteStatus) {

        return productService.updateDeleteStatus(ids,deleteStatus);
    }






}
