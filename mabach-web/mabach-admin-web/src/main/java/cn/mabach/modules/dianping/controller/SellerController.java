package cn.mabach.modules.dianping.controller;

import java.util.Arrays;
import java.util.Date;
import java.util.Map;

import cn.mabach.dianping.entity.SellerEntity;
import cn.mabach.modules.dianping.feign.SellerServiceFeign;
import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import cn.mabach.utils.TypeCastUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.protocol.types.Field;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;



/**
 * 
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-02-28 16:53:43
 */
@Slf4j
@RestController
@RequestMapping("/seller")
public class SellerController {
    @Autowired
    private SellerServiceFeign sellerService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public PageResult list(@RequestParam(value = "keyword", required = false) String keyword,
                   @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                   @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize){
        PageResult page = sellerService.queryPage(keyword,pageNum,pageSize);

        return page;
    }


    /**
     * 信息
     */
    @RequestMapping("/{id}")
    public SellerEntity info(@PathVariable("id") Integer id){
		SellerEntity seller = sellerService.getByIdE(id);

        return seller;
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public RS save(@RequestBody SellerEntity seller){
        seller.setCreatedTime(new Date());
        seller.setUpdatedTime(new Date());
		sellerService.saveE(seller);

        return RS.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update/{id}")
    public RS update(@PathVariable("id") Integer id,@RequestBody SellerEntity seller){
        seller.setId(id);
        seller.setCreatedTime(new Date());
        seller.setUpdatedTime(new Date());
		sellerService.updateByIdE(seller);

        return RS.ok();
    }

    /**
     * 根据id删除
     */
    @RequestMapping("/delete/{id}")
    public RS delete(@PathVariable("id") Integer id){
        sellerService.removeOneById(id);

        return RS.ok();
    }

    /**
     * 根据id集合删除
     */
    @RequestMapping("/delete")
    public RS delete(@RequestBody Integer[] ids){
		sellerService.removeByIdsE(Arrays.asList(ids));
        return RS.ok();
    }


    /**
     * 修改禁用
     */
    @RequestMapping("/update/showStatus")
    public RS updateStatus(@RequestParam("id") Integer id,@RequestParam("disabledFlag") Integer disabledFlag){

//        log.info("id:{}",id);
        SellerEntity entity = new SellerEntity();
        entity.setId(id);
        entity.setDisabledFlag(disabledFlag);
        sellerService.updateByIdE(entity);


        return RS.ok();
    }


}
