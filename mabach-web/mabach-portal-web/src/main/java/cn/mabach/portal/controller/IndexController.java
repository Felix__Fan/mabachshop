package cn.mabach.portal.controller;

import cn.mabach.exception.RRException;
import cn.mabach.portal.feign.MemberServiceFeign;
import cn.mabach.member.entity.MemberEntity;

import cn.mabach.result.RS;
import cn.mabach.utils.JwtDecode;
import com.alibaba.fastjson.JSON;
import com.netflix.ribbon.proxy.annotation.Http;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.data.redis.core.StringRedisTemplate;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @Author: ming
 * @Date: 2020/2/10 0010 下午 9:00
 */
@Controller
@RequestMapping("/portal")
@Slf4j
public class IndexController {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private MemberServiceFeign memberServiceFeign;
    @Autowired
    private PasswordEncoder passwordEncoder;

    private static final String FROM_URL="http://www.mabach.cn/api-portal/portal/info";





    @GetMapping("/info")
    public String toIndex(Model model, HttpServletRequest request){
        String authorization = request.getHeader("Authorization");
        String jwt = authorization.replace("Bearer ", "");
        Map<String ,String> map=null;
        try {
            String decode = JwtDecode.decode(jwt);
             map= JSON.parseObject(decode, Map.class);
        } catch (Exception e) {
            throw new RRException("令牌错误");
        }
        String user_name = map.get("user_name");
        log.info("user_name:{}",user_name);
        model.addAttribute("member",user_name);

        return "info";
    }


    @RequestMapping("/register")
    private String register(@RequestParam(value = "from",required = false,defaultValue = "") String from, Model model){

        if (StringUtils.isEmpty(from)){
            model.addAttribute("from",FROM_URL);
        }
        model.addAttribute("from",from);
        return "register";

    }
//注册账号
    @RequestMapping("/signin")
    @ResponseBody
    private RS signin(@RequestParam("username") String username, @RequestParam("password") String password,
                      @RequestParam("phone") String phone, @RequestParam("code") String code,
                      HttpServletRequest request){



       ;
        String s = stringRedisTemplate.boundValueOps(phone).get();
        log.info("phone:{}",phone);
        log.info("redis里的验证码:{}",s);
        log.info("传进来的验证码:{}",code);
        if (!code.equals(s)){
            return RS.error("手机号或验证码不正确");
        }

        RS<MemberEntity> memberByUsername = memberServiceFeign.getMemberByUsername(username);

        if (memberByUsername.getData()!=null){
            return RS.error("该账号已经存在");
        }
        RS<MemberEntity> byMobile = memberServiceFeign.getByMobile(phone);
        if (byMobile.getData()!=null){
            return RS.error("该手机号已经存在");
        }
        MemberEntity entity = new MemberEntity();
        entity.setUsername(username);
        entity.setPassword(passwordEncoder.encode(password));
        entity.setPhone(phone);

        RS rs = memberServiceFeign.saveE(entity);
        if (!rs.isFlag()){
            return RS.error("注册失败");
        }




        return RS.ok("注册成功");

    }
    @RequestMapping("/prelogin")
    private String preLogin(@RequestParam("phone") String phone,@RequestParam("code") String code,
                            @RequestParam(value = "from",required = false) String from,Model model){

        model.addAttribute("mobile",phone);
        model.addAttribute("code",code);
        if (StringUtils.isEmpty(from)){
            model.addAttribute("from",FROM_URL);
        }
        model.addAttribute("from",from);

        return "preLogin";

    }

    @PostMapping("/getcurrentuser")
    @ResponseBody
    public RS getUser(HttpServletRequest request){
//        String user = memberServiceFeign.getUser(request);
//        if (StringUtils.isEmpty(user)) {
//            return RS.error("无法获取用户");
//        }
        return RS.ok("获取成功","张三") ;

    }


}
