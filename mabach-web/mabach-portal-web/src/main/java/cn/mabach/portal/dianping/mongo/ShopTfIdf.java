package cn.mabach.portal.dianping.mongo;

import lombok.Data;

import java.util.List;


/**
 * @Author: ming
 * @Date: 2020/3/26 0026 下午 9:25
 */
@Data
public class ShopTfIdf {
    private Integer _id;
    private List<WordScore> wordScore;
}
