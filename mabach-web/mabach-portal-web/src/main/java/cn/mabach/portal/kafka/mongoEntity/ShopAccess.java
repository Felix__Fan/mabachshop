package cn.mabach.portal.kafka.mongoEntity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author: ming
 * @Date: 2020/3/26 0026 下午 6:18
 */
@Data
public class ShopAccess implements Serializable {

    private Boolean click=false;
    private Boolean collect=false ;
    private Boolean share=false ;
    private Boolean buy =false;
    private Boolean pay =false;
    private Long date;

}
