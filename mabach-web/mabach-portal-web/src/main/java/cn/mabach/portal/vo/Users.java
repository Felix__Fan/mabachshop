package cn.mabach.portal.vo;


import lombok.Data;

@Data
public class Users {

    private String id;

    /**
     * 用户名，账号，慕信号
     */
    private String username;

    /**
     * 密码
     */
    private String password;



    /**
     * 昵称
     */
    private String nickname;

    /**
     * 新用户注册后默认后台生成二维码，并且上传到fastdfs
     */
    private String icon;
    private String iconBig;
    private String qrcode;
    private String cid;


}