package cn.mabach.portal.dianping.mongo;

import io.swagger.models.auth.In;
import lombok.Data;

/**
 * @Author: ming
 * @Date: 2020/3/26 0026 上午 1:40
 */
@Data
public class Featrue {
    private Integer mid;
    private Double weight;
}
