package cn.mabach.portal.dianping.mongo;

import lombok.Data;

/**
 * @Author: ming
 * @Date: 2020/3/25 0025 下午 2:29
 */
@Data
public class Recs {
    private Integer _id;
    private Double score;
}
