import http from '@/utils/httpRequest'
export function fetchList(params) {
  return http({
    url:http.adornUrl('/home/brand/list'),
    method:'get',
    params:params
  })
}

export function updateRecommendStatus(params) {
  return http({
    url:http.adornUrl('/home/brand/update/recommendStatus'),
    method:'post',
    params:params
  })
}

export function deleteHomeBrand(params) {
  return http({
    url:http.adornUrl('/home/brand/delete'),
    method:'post',
    params:params
  })
}

export function createHomeBrand(data) {
  return http({
    url:http.adornUrl('/home/brand/create'),
    method:'post',
    data:data
  })
}

export function updateHomeBrandSort(params) {
  return http({
    url:http.adornUrl('/home/brand/update/sort/'),
    method:'post',
    params:params
  })
}
