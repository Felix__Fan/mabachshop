import http from '@/utils/httpRequest'
export function fetchList(params) {
  return http({
    url:http.adornUrl('/home/advertise/list'),
    method:'get',
    params:params
  })
}
export function updateStatus(params) {
  return http({
    url:http.adornUrl('/home/advertise/update/status/'),
    method:'post',
    params:params
  })
}
export function deleteHomeAdvertise(params) {
  return http({
    url:http.adornUrl('/home/advertise/delete'),
    method:'post',
    params:params
  })
}
export function createHomeAdvertise(data) {
  return http({
    url:http.adornUrl('/home/advertise/create'),
    method:'post',
    data:data
  })
}
export function getHomeAdvertise(id) {
  return http({
    url:http.adornUrl('/home/advertise/'+id),
    method:'get',
  })
}

export function updateHomeAdvertise(id,data) {
  return http({
    url:http.adornUrl('/home/advertise/update/'+id),
    method:'post',
    data:data
  })
}
