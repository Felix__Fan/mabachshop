import http from '@/utils/httpRequest'

export function fetchList(params) {
  return http({
    url:http.adornUrl('/store/list'),
    method:'get',
    params:params
  })
}
export function createBrand(data) {
  return http({
    url:http.adornUrl('/store/save'),
    method:'post',
    data:data
  })
}

export function analysis(data) {
  return http({
    url:http.adornUrl('/store/getAdsress'),
    method:'get',
    params:data
  })
}




export function deleteBrand(id) {
  return http({
    url:http.adornUrl('/store/delete/'+id),
    method:'get',
  })
}

export function getBrand(id) {
  return http({
    url:http.adornUrl('/store/'+id),
    method:'get',
  })
}

export function updateBrand(id,data) {
  return http({
    url:http.adornUrl('/store/update/'+id),
    method:'post',
    data:data
  })
}

