import http from '@/utils/httpRequest'

// 获取路由
export const getRouters = () => {
  return http({
    url:http.adornUrl('/getRouters'),
    method: 'get'
  })
}