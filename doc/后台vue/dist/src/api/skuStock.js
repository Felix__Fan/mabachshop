import http from '@/utils/httpRequest'
export function fetchList(pid,params) {
  return http({
    url:http.adornUrl('/sku/'+pid),
    method:'get',
    params:params
  })
}

export function update(pid,data) {
  return http({
    url:http.adornUrl('/sku/update/'+pid),
    method:'post',
    data:data
  })
}
