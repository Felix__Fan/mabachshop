import http from '@/utils/httpRequest'
export function fetchList(params) {
  return http({
    url:http.adornUrl('/returnApply/list'),
    method:'get',
    params:params
  })
}

export function deleteApply(params) {
  return http({
    url:http.adornUrl('/returnApply/delete'),
    method:'post',
    params:params
  })
}
export function updateApplyStatus(id,data) {
  return http({
    url:http.adornUrl('/returnApply/update/status/'+id),
    method:'post',
    data:data
  })
}

export function getApplyDetail(id) {
  return http({
    url:http.adornUrl('/returnApply/'+id),
    method:'get'
  })
}
