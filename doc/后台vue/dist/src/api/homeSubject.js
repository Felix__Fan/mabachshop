import http from '@/utils/httpRequest'
export function fetchList(params) {
  return http({
    url:http.adornUrl('/home/recommendSubject/list'),
    method:'get',
    params:params
  })
}

export function updateRecommendStatus(params) {
  return http({
    url:http.adornUrl('/home/recommendSubject/update/recommendStatus'),
    method:'post',
    params:params
  })
}

export function deleteHomeSubject(params) {
  return http({
    url:http.adornUrl('/home/recommendSubject/delete'),
    method:'post',
    params:params
  })
}

export function createHomeSubject(data) {
  return http({
    url:http.adornUrl('/home/recommendSubject/create'),
    method:'post',
    data:data
  })
}

export function updateHomeSubjectSort(params) {
  return http({
    url:http.adornUrl('/home/recommendSubject/update/sort/'),
    method:'post',
    params:params
  })
}
