import http from '@/utils/httpRequest'
export function fetchList(params) {
  return http({
    url:http.adornUrl('/returnReason/list'),
    method:'get',
    params:params
  })
}

export function deleteReason(params) {
  return http({
    url:http.adornUrl('/returnReason/delete'),
    method:'post',
    params:params
  })
}

export function updateStatus(params) {
  return http({
    url:http.adornUrl('/returnReason/update/status'),
    method:'post',
    params:params
  })
}

export function addReason(data) {
  return http({
    url:http.adornUrl('/returnReason/create'),
    method:'post',
    data:data
  })
}

export function getReasonDetail(id) {
  return http({
    url:http.adornUrl('/returnReason/'+id),
    method:'get'
  })
}

export function updateReason(id,data) {
  return http({
    url:http.adornUrl('/returnReason/update/'+id),
    method:'post',
    data:data
  })
}
