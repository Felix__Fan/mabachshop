import http from '@/utils/httpRequest'

export function fetchList(params) {
  return http({
    url:http.adornUrl('/flashSession/list'),
    method: 'get',
    params: params
  })
}

export function fetchSelectList(params) {
  return http({
    url:http.adornUrl('/flashSession/selectList'),
    method: 'get',
    params: params
  })
}

export function updateStatus(id, params) {
  return http({
    url:http.adornUrl('/flashSession/update/status/' + id),
    method: 'post',
    params: params
  })
}

export function deleteSession(id) {
  return http({
    url:http.adornUrl('/flashSession/delete/' + id),
    method: 'post'
  })
}

export function createSession(data) {
  return http({
    url:http.adornUrl('/flashSession/create'),
    method: 'post',
    data: data
  })
}

export function updateSession(id, data) {
  return http({
    url:http.adornUrl('/flashSession/update/' + id),
    method: 'post',
    data: data
  })
}
