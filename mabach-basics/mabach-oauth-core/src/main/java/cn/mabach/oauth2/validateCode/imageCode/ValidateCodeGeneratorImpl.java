package cn.mabach.oauth2.validateCode.imageCode;

import com.google.code.kaptcha.impl.DefaultKaptcha;
import org.springframework.beans.factory.annotation.Autowired;

import java.awt.image.BufferedImage;

/**
 * @Author: ming
 * @Date: 2020/2/2 0002 下午 2:22
 */
public class ValidateCodeGeneratorImpl implements ValidateCodeGenerator {

    @Autowired
    private DefaultKaptcha defaultKaptcha;

    @Override
    public BufferedImage createCode(String code) {
        return defaultKaptcha.createImage(code);
    }
}
