package cn.mabach.oauth2.utils;

import lombok.Data;

import java.io.Serializable;

//import org.apache.http.HttpStatus;


@Data
public class RS<T> implements Serializable {
    private boolean flag;
    private Integer code;
    private String message;
    private T data;

    public RS() {
    }

    public RS(boolean flag, Integer code, String message, Object data) {
        this.flag = flag;
        this.code = code;
        this.message = message;
        this.data = (T)data;
    }

    public RS(boolean flag, Integer code, String message) {
        this.flag = flag;
        this.code = code;
        this.message = message;
    }

    public static RS ok(){
        return new RS(true,200,"操作成功");
    }

    public static RS ok(String message){
        return new RS(true,200,message);
    }
    public static RS ok(Integer code,String message){
        return new RS(true,code,message);
    }
    public static RS ok(String message,Object data){
        return new RS(true,200,message,data);
    }

    public static RS ok(Object data){
        return new RS(true,200,"操作成功",data);
    }
    public static RS error(){
        return new RS(false,500,"操作失败");
    }

    public static RS error(String message){
        return new RS(false,500,message);
    }

    public static RS error(Integer code,String message){
        return new RS(false,code,message);
    }
}
