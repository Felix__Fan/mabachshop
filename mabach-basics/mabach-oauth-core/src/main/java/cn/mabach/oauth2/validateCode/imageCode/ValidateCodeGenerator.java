package cn.mabach.oauth2.validateCode.imageCode;


import java.awt.image.BufferedImage;

public interface ValidateCodeGenerator {

	BufferedImage createCode(String code);
	
}