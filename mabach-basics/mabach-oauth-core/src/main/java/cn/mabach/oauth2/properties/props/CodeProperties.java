package cn.mabach.oauth2.properties.props;

import lombok.Data;

@Data
public class CodeProperties {
	
	private ImageProperties image = new ImageProperties();


	private SmsCodeProperties smsCode=new SmsCodeProperties();
	
}