package cn.mabach.oauth2.project.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author: ming
 * @Date: 2020/2/4 0004 下午 5:19
 */
@Data
public class SocialUser implements Serializable {
    private String provider;
    private String openId;
    private String nickNake;
    private String headImg;
}
