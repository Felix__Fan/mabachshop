package cn.mabach.member.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2020-02-24 21:58:59
 */
@Data
@TableName("send_message")
public class SendMessageEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
		@TableId
	
private Long id;
	/**
	 * 
	 */
	
private Long sendUserId;
	/**
	 * 
	 */
	
private Long receiveUserId;
	/**
	 * 
	 */
	
private Date requestTime;

}
