package cn.mabach.business.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 优惠券和产品的关系表
 * 
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2020-01-02 19:26:19
 */
@Data
@TableName("sms_coupon_product_relation")
public class CouponProductRelationEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
		@TableId
		private Long id;
	/**
	 * 
	 */
		private Long couponId;
	/**
	 * 
	 */
		private Long productId;
	/**
	 * 商品名称
	 */
		private String productName;
	/**
	 * 商品编码
	 */
		private String productSn;

}
