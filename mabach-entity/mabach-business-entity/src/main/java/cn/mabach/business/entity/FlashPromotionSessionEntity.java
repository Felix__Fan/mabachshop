package cn.mabach.business.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;


import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 限时购场次表
 * 
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2020-01-02 19:26:24
 */
@Data
@TableName("sms_flash_promotion_session")
public class FlashPromotionSessionEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 编号
	 */
		@TableId
		private Long id;
	/**
	 * 场次名称
	 */
		private String name;
	/**
	 * 每日开始时间
	 */

		private Time startTime;
	/**
	 * 每日结束时间
	 */

		private Time endTime;
	/**
	 * 启用状态：0->不启用；1->启用
	 */
		private Integer status;
	/**
	 * 创建时间
	 */

	private Date createTime;

}
