package cn.mabach.cms.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * 帮助表
 * 
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-28 16:25:19
 */
@Data
@TableName("cms_help")
public class HelpEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
		@TableId
		private Long id;
	/**
	 * 
	 */
		private Long categoryId;
	/**
	 * 
	 */
		private String icon;
	/**
	 * 
	 */
		private String title;
	/**
	 * 
	 */
		private Integer showStatus;
	/**
	 * 
	 */

	private Date createTime;
	/**
	 * 
	 */
		private Integer readCount;
	/**
	 * 
	 */
		private String content;

}
