package cn.mabach.cms.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * 专题评论表
 * 
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-28 16:24:45
 */
@Data
@TableName("cms_topic_comment")
public class TopicCommentEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
		@TableId
		private Long id;
	/**
	 * 
	 */
		private String memberNickName;
	/**
	 * 
	 */
		private Long topicId;
	/**
	 * 
	 */
		private String memberIcon;
	/**
	 * 
	 */
		private String content;
	/**
	 * 
	 */

	private Date createTime;
	/**
	 * 
	 */
		private Integer showStatus;

}
