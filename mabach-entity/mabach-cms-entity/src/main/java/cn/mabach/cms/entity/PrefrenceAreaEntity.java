package cn.mabach.cms.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 优选专区
 * 
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-28 16:24:45
 */
@Data
@TableName("cms_prefrence_area")
public class PrefrenceAreaEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
		@TableId
		private Long id;
	/**
	 * 
	 */
		private String name;
	/**
	 * 
	 */
		private String subTitle;
	/**
	 * 展示图片
	 */
		private byte[] pic;
	/**
	 * 
	 */
		private Integer sort;
	/**
	 * 
	 */
		private Integer showStatus;

}
