package cn.mabach.goods.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 商品会员价格表
 * 
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-28 16:29:32
 */
@Data
@TableName("tb_member_price")
public class MemberPriceEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
		@TableId
		private Long id;
	/**
	 * 
	 */
		private Long productId;
	/**
	 * 
	 */
		private Long memberLevelId;
	/**
	 * 会员价格
	 */
		private BigDecimal memberPrice;
	/**
	 * 
	 */
		private String memberLevelName;

}
