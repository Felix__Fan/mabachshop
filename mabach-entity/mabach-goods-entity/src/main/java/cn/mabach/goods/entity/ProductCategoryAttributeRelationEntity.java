package cn.mabach.goods.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 产品的分类和属性的关系表，用于设置分类筛选条件（只支持一级分类）
 * 
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-28 16:28:40
 */
@Data
@TableName("tb_product_category_attribute_relation")
public class ProductCategoryAttributeRelationEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
		@TableId
		private Long id;
	/**
	 * 
	 */
		private Long productCategoryId;
	/**
	 * 
	 */
		private Long productAttributeId;

}
