package cn.mabach.goods.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 存储产品参数信息的表
 * 
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-28 16:28:40
 */
@Data
@TableName("tb_product_attribute_value")
public class ProductAttributeValueEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
		@TableId
		private Long id;
	/**
	 * 
	 */
		private Long productId;
	/**
	 * 
	 */
		private Long productAttributeId;
	/**
	 * 手动添加规格或参数的值，参数单值，规格有多个时以逗号隔开
	 */
		private String value;

}
