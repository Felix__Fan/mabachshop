package cn.mabach.goods.dto;

import cn.mabach.cms.entity.PrefrenceAreaProductRelationEntity;
import cn.mabach.cms.entity.SubjectProductRelationEntity;

import cn.mabach.goods.entity.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class ProductParam extends ProductEntity {
    @ApiModelProperty("商品阶梯价格设置")
    private List<ProductLadderEntity> productLadderList;
    @ApiModelProperty("商品满减价格设置")
    private List<ProductFullReductionEntity> productFullReductionList;
    @ApiModelProperty("商品会员价格设置")
    private List<MemberPriceEntity> memberPriceList;
    @ApiModelProperty("商品的sku库存信息")
    private List<SkuStockEntity> skuStockList;
    @ApiModelProperty("商品参数及自定义规格属性")
    private List<ProductAttributeValueEntity> productAttributeValueList;
    @ApiModelProperty("专题和商品关系")
    private List<SubjectProductRelationEntity> subjectProductRelationList;
    @ApiModelProperty("优选专区和商品的关系")
    private List<PrefrenceAreaProductRelationEntity> prefrenceAreaProductRelationList;
}
