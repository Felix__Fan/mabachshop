package cn.mabach.business.dto;

import cn.mabach.business.entity.FlashPromotionProductRelationEntity;
import cn.mabach.goods.entity.ProductEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class FlashPromotionProduct extends FlashPromotionProductRelationEntity {
    @ApiModelProperty(value = "秒杀活动的商品")
    private ProductEntity product;
}
