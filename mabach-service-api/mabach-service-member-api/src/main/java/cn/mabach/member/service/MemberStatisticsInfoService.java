package cn.mabach.member.service;


import cn.mabach.member.entity.MemberStatisticsInfoEntity;
import cn.mabach.result.RS;
import cn.mabach.result.TableDataInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;
/**
 * 会员统计信息
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2020-01-04 17:30:47
 */
@Api(tags = "会员静态信息")
public interface MemberStatisticsInfoService   {

    @GetMapping("/listMemberStatisticsInfo")
    @ApiOperation(value = "分页查询")
    TableDataInfo queryPage(@RequestParam(value = "consumeAmount", required = false) BigDecimal consumeAmount,
                            @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                            @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize);



    @GetMapping("/infoMemberStatisticsInfo")
    @ApiOperation(value = "根据id查找")
    RS<MemberStatisticsInfoEntity> getByIdE(@RequestParam("id") Long id);


    @PostMapping("/saveMemberStatisticsInfo")
    @ApiOperation(value = "保存")
    RS saveE(@RequestBody MemberStatisticsInfoEntity entity);


    @PutMapping("/updateMemberStatisticsInfo")
    @ApiOperation(value = "修改")
    RS updateByIdE(@RequestBody MemberStatisticsInfoEntity entity);

    @DeleteMapping("/deleteMemberStatisticsInfos")
    @ApiOperation(value = "根据ID集合删除")
    RS removeByIdsE(@RequestParam("ids") List<Long> ids);


}

