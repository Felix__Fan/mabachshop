package cn.mabach.member.service;


import cn.mabach.member.entity.MemberEntity;
import cn.mabach.result.RS;
import cn.mabach.result.TableDataInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
/**
 * 会员表
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2020-01-04 15:47:34
 */

@Api(tags = "会员服务接口")
public interface MemberService   {

    @PostMapping("/getUserToken")
    String getUser(HttpServletRequest request);

    @GetMapping("/listMember")
    @ApiOperation(value = "分页查询")
    TableDataInfo queryPage(@RequestParam(value = "username", required = false) String username,
                            @RequestParam(value = "phone", required = false) String phone,
                            @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                            @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize);



    @GetMapping("/infoMember")
    @ApiOperation(value = "根据id查找")
    RS<MemberEntity> getByIdE(@RequestParam("id") Long id);

    @GetMapping("/infoMemberByqqOpenId")
    @ApiOperation(value = "根据openId查找")
    RS<MemberEntity> getByIByqqOpenId(@RequestParam("openId") String openId);

    @GetMapping("/infoMemberByUsername")
    @ApiOperation(value = "根据用户名查找")
    RS<MemberEntity> getMemberByUsername(@RequestParam("username") String username);

    @GetMapping("/infoMemberByMobile")
    @ApiOperation(value = "根据手机号查找用户")
    RS<MemberEntity> getByMobile(@RequestParam("mobile") String mobile);

    @PostMapping("/saveMember")
    @ApiOperation(value = "保存")
    RS saveE(@RequestBody MemberEntity entity);

    @PostMapping("/updateMember")
    @ApiOperation(value = "修改")
    RS updateByIdE(@RequestBody MemberEntity entity);

    @PostMapping("/deleteMembers")
    @ApiOperation(value = "根据ID集合删除")
    RS removeByIdsE(@RequestParam("ids") List<Long> ids);

    @GetMapping("/infoPermsByUsername")
    @ApiOperation(value = "根据用户名查找权限")
    RS<List<String>> getPermsByUsername(@RequestParam("username") String username);

    @GetMapping("/saveMemberByOpenId")
    @ApiOperation(value = "关联qq保存")
    RS saveByOpenId(@RequestParam("username") String username,@RequestParam("password") String password,@RequestParam("openId") String openId);


}

