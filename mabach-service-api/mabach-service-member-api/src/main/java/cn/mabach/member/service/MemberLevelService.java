package cn.mabach.member.service;


import cn.mabach.member.entity.MemberLevelEntity;
import cn.mabach.result.RS;
import cn.mabach.result.TableDataInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.List;
/**
 * 会员等级表
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2020-01-04 17:30:47
 */
@Api(tags = "会员等级")
public interface MemberLevelService   {

    @GetMapping("/listMemberLevel")
    @ApiOperation(value = "分页查询")
    TableDataInfo queryPage(@RequestParam(value = "keyword", required = false) String keyword,
                            @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                            @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize);



    @GetMapping("/infoMemberLevel")
    @ApiOperation(value = "根据id查找")
    RS<MemberLevelEntity> getByIdE(@RequestParam("id") Long id);


    @PostMapping("/saveMemberLevel")
    @ApiOperation(value = "保存")
    RS saveE(@RequestBody MemberLevelEntity entity);


    @PutMapping("/updateMemberLevel")
    @ApiOperation(value = "修改")
    RS updateByIdE(@RequestBody MemberLevelEntity entity);

    @DeleteMapping("/deleteMemberLevels")
    @ApiOperation(value = "根据ID集合删除")
    RS removeByIdsE(@RequestParam("ids") List<Long> ids);


}

