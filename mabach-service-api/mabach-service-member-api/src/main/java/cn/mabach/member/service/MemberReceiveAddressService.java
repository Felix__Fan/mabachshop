package cn.mabach.member.service;


import cn.mabach.member.entity.MemberReceiveAddressEntity;

import cn.mabach.result.RS;
import cn.mabach.result.TableDataInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.List;
/**
 * 会员收货地址表
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2020-01-04 17:30:47
 */
@Api(tags = "会员地址")
public interface MemberReceiveAddressService   {

    @GetMapping("/listMemberReceiveAddress")
    @ApiOperation(value = "分页查询")
    TableDataInfo queryPage(@RequestParam(value = "name", required = false) String name,
                            @RequestParam(value = "phoneNumber", required = false) String phoneNumber,
                            @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                            @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize);



    @GetMapping("/infoMemberReceiveAddress")
//    @ApiOperation(value = "根据id查找")
    RS<MemberReceiveAddressEntity> getByIdE(@RequestParam("id") Long id);


    @PostMapping("/saveMemberReceiveAddress")
    @ApiOperation(value = "保存")
    RS saveE(@RequestBody MemberReceiveAddressEntity entity);


    @PutMapping("/updateMemberReceiveAddress")
    @ApiOperation(value = "修改")
    RS updateByIdE(@RequestBody MemberReceiveAddressEntity entity);

    @DeleteMapping("/deleteMemberReceiveAddresss")
    @ApiOperation(value = "根据ID集合删除")
    RS removeByIdsE(@RequestParam("ids") List<Long> ids);


}

