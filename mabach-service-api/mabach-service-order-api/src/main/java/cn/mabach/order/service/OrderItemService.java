package cn.mabach.order.service;


import cn.mabach.order.entity.OrderItemEntity;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;
/**
 * 订单中所包含的商品
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2020-01-02 23:46:11
 */
@FeignClient(name = "app-mabach-order")
@Api(tags = "广告服务接口")
public interface OrderItemService   {

    @GetMapping("/listOrderItem")
    @ApiOperation(value = "分页查询")
    RS<PageResult<OrderItemEntity>> queryPage(@RequestParam(value = "keyword", required = false) String keyword,
                                              @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                              @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize);



    @GetMapping("/infoOrderItem")
    @ApiOperation(value = "根据id查找")
    RS<OrderItemEntity> getByIdE(@RequestParam("id") Long id);

    @PostMapping("/saveOrderItem")
    @ApiOperation(value = "保存")
    RS saveE(@RequestBody OrderItemEntity entity);

    @PostMapping("/updateOrderItem")
    @ApiOperation(value = "修改")
    RS updateByIdE(@RequestBody OrderItemEntity entity);

    @PostMapping("/deleteOrderItems")
    @ApiOperation(value = "根据ID集合删除")
    RS removeByIdsE(@RequestParam("ids") List<Long> ids);

    @ApiOperation("单个删除")
    @RequestMapping(value = "/deleteOrderItemByid", method = RequestMethod.POST)
    public RS removeByIdE(@RequestParam("id") Long id) ;

    @GetMapping("/infoOrderItemOrderById")
    @ApiOperation(value = "根据订单id查找")
    List<OrderItemEntity> getOrderById(@RequestParam("id") Long id);
}

