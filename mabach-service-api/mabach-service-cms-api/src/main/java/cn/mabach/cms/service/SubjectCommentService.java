package cn.mabach.cms.service;


import cn.mabach.cms.entity.SubjectCommentEntity;
import cn.mabach.result.RS;
import cn.mabach.result.TableDataInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.List;
/**
 * 专题评论表
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2020-02-13 22:18:23
 */
@Api(tags = "广告服务接口")
public interface SubjectCommentService   {

    @GetMapping("/listSubjectComment")
    @ApiOperation(value = "分页查询")
    TableDataInfo queryPage(@RequestParam(value = "keyword", required = false) String keyword,
                            @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                            @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize);



    @GetMapping("/infoSubjectComment")
    @ApiOperation(value = "根据id查找")
    RS<SubjectCommentEntity> getByIdE(@RequestParam("id") Long id);


    @PostMapping("/saveSubjectComment")
    @ApiOperation(value = "保存")
    RS saveE(@RequestBody SubjectCommentEntity entity);


    @PutMapping("/updateSubjectComment")
    @ApiOperation(value = "修改")
    RS updateByIdE(@RequestBody SubjectCommentEntity entity);

    @DeleteMapping("/deleteSubjectComments")
    @ApiOperation(value = "根据ID集合删除")
    RS removeByIdsE(@RequestParam("ids") List<Long> ids);


}

