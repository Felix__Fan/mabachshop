package cn.mabach.cms.service;



import cn.mabach.cms.entity.TopicEntity;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;
/**
 * 话题表
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-27 20:51:42
 */
@Api(tags = "广告服务接口")
@RequestMapping("/")
@FeignClient(name="app-mabach-cms")
public interface TopicService   {

    @GetMapping("/listTopic")
    @ApiOperation(value = "分页查询")
    RS<PageResult<TopicEntity>> queryPage(@RequestParam(value = "keyword", required = false) String keyword,
                                          @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                          @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize);



    @GetMapping("/infoTopic")
    @ApiOperation(value = "根据id查找")
    RS<TopicEntity> getByIdE(@RequestParam("id") Long id);

    @PostMapping("/saveTopic")
    @ApiOperation(value = "保存")
    RS saveE(@RequestBody TopicEntity entity);

    @PostMapping("/updateTopic")
    @ApiOperation(value = "修改")
    RS updateByIdE(@RequestBody TopicEntity entity);

    @PostMapping("/deleteOneTopic")
    @ApiOperation(value = "根据ID删除")
    RS removeOneById(@RequestParam("id") Long id);

    @PostMapping("/deleteTopic")
    @ApiOperation(value = "根据ID集合删除")
    RS removeByIdsE(@RequestBody List<Long> ids);
}

