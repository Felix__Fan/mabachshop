package cn.mabach.goods.service;

import cn.mabach.goods.dto.ProductAttrInfo;
import cn.mabach.goods.entity.ProductAttributeEntity;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 商品属性参数表
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-26 01:04:21
 */
@Api(tags = "PmsProductAttributeController", description = "商品属性管理")
public interface ProductAttributeService   {

    @GetMapping("/listProductAttribute")
    @ApiOperation("根据分类查询属性列表或参数列表")
    @ApiImplicitParams({@ApiImplicitParam(name = "type", value = "0表示属性，1表示参数", required = true, paramType = "query", dataType = "integer")})
    RS<PageResult<ProductAttributeEntity>> queryPage(@RequestParam("cid") Long cid,
                                                     @RequestParam(value = "type",required = false) Integer type,
                                                     @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                     @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize);



    @GetMapping("/infoProductAttribute")
    @ApiOperation("查询单个商品属性")
    RS<ProductAttributeEntity> getByIdE(@RequestParam("id") Long id);

    @PostMapping("/saveProductAttribute")
    @ApiOperation("添加商品属性信息")
    RS saveE(@RequestBody ProductAttributeEntity entity);

    @PostMapping("/updateProductAttribute")
    @ApiOperation("修改商品属性信息")
    RS updateByIdE(@RequestBody ProductAttributeEntity entity);

    @PostMapping("/deleteOneProductAttribute")
    @ApiOperation(value = "根据ID删除")
    RS removeOneById(@RequestParam("id") Long id);

    @PostMapping("/deleteProductAttribute")
    @ApiOperation(value = "根据ID集合删除")
    RS removeByIdsE(@RequestParam("ids") List<Long> ids);

    @ApiOperation("根据商品分类的id获取商品属性及属性分类")
    @GetMapping(value = "/attrInfo/productCategoryId")
    public RS<List<ProductAttrInfo>> getAttrInfo(@RequestParam("productCategoryId") Long productCategoryId);
}

