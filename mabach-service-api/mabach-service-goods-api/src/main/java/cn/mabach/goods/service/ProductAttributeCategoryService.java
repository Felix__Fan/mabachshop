package cn.mabach.goods.service;

import cn.mabach.goods.entity.ProductAttributeCategoryEntity;
import cn.mabach.goods.dto.ProductAttributeCategoryItem;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 产品属性分类表
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-26 01:03:48
 */
@Api(tags = "ProductAttributeCategoryService", description = "商品属性分类管理")
public interface ProductAttributeCategoryService   {

    @GetMapping("/listProductAttributeCategory")
    @ApiOperation(value = "分页查询")
    RS<PageResult<ProductAttributeCategoryEntity>> queryPage(
                                                             @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                             @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize);



    @GetMapping("/infoProductAttributeCategory")
    @ApiOperation(value = "根据id查找")
    RS<ProductAttributeCategoryEntity> getByIdE(@RequestParam("id") Long id);

    @PostMapping("/saveProductAttributeCategory")
    @ApiOperation("添加商品属性分类")
    RS saveE(@RequestBody ProductAttributeCategoryEntity entity);

    @PostMapping("/updateProductAttributeCategory")
    @ApiOperation(value = "修改")
    RS updateByIdE(@RequestBody ProductAttributeCategoryEntity entity);

    @PostMapping("/deleteOneProductAttributeCategory")
    @ApiOperation(value = "根据ID删除")
    RS removeOneById(@RequestParam("id") Long id);

    @PostMapping("/deleteProductAttributeCategory")
    @ApiOperation(value = "根据ID集合删除")
    RS removeByIdsE(@RequestBody List<Long> ids);

    @ApiOperation("获取所有商品属性分类及其下属性")
    @RequestMapping(value = "/list/withAttrProductAttributeCategory", method = RequestMethod.GET)
    public RS<List<ProductAttributeCategoryItem>> getListWithAttr();
}

