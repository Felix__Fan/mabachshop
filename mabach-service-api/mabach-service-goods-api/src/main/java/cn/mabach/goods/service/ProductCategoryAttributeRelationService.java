package cn.mabach.goods.service;

import cn.mabach.goods.entity.ProductCategoryAttributeRelationEntity;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * 产品的分类和属性的关系表，用于设置分类筛选条件（只支持一级分类）
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-26 01:03:48
 */
@Api(tags = "广告服务接口")
public interface ProductCategoryAttributeRelationService   {

    @GetMapping("/listProductCategoryAttributeRelation")
    @ApiOperation(value = "分页查询")
    RS<PageResult<ProductCategoryAttributeRelationEntity>> queryPage(@RequestParam(value = "keyword", required = false) String keyword,
                                                                     @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                                     @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize);


    @GetMapping("/infoProductCategoryAttributeRelation")
    @ApiOperation(value = "根据id查找")
    RS<ProductCategoryAttributeRelationEntity> getByIdE(@RequestParam("id") Long id);

    @PostMapping("/saveProductCategoryAttributeRelation")
    @ApiOperation(value = "保存")
    RS saveE(@RequestBody ProductCategoryAttributeRelationEntity entity);

    @PostMapping("/updateProductCategoryAttributeRelation")
    @ApiOperation(value = "修改")
    RS updateByIdE(@RequestBody ProductCategoryAttributeRelationEntity entity);

    @PostMapping("/deleteOneProductCategoryAttributeRelation")
    @ApiOperation(value = "根据ID删除")
    RS removeOneById(@RequestParam("id") Long id);

    @PostMapping("/deleteProductCategoryAttributeRelation")
    @ApiOperation(value = "根据ID集合删除")
    RS removeByIdsE(@RequestBody List<Long> ids);
}

