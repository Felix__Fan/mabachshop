package cn.mabach.goods.service;

import cn.mabach.goods.entity.ProductVertifyRecordEntity;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * 商品审核记录
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-26 01:03:48
 */
@Api(tags = "广告服务接口")
public interface ProductVertifyRecordService   {

    @GetMapping("/listProductVertifyRecord")
    @ApiOperation(value = "分页查询")
    RS<PageResult<ProductVertifyRecordEntity>> queryPage(@RequestParam(value = "keyword", required = false) String keyword,
                                                         @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                         @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize);



    @GetMapping("/infoProductVertifyRecord")
    @ApiOperation(value = "根据id查找")
    RS<ProductVertifyRecordEntity> getByIdE(@RequestParam("id") Long id);

    @PostMapping("/saveProductVertifyRecord")
    @ApiOperation(value = "保存")
    RS saveE(@RequestBody ProductVertifyRecordEntity entity);

    @PostMapping("/updateProductVertifyRecord")
    @ApiOperation(value = "修改")
    RS updateByIdE(@RequestBody ProductVertifyRecordEntity entity);

    @PostMapping("/deleteOneProductVertifyRecord")
    @ApiOperation(value = "根据ID删除")
    RS removeOneById(@RequestParam("id") Long id);

    @PostMapping("/deleteProductVertifyRecord")
    @ApiOperation(value = "根据ID集合删除")
    RS removeByIdsE(@RequestBody List<Long> ids);
}

