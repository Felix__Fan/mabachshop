package cn.mabach.goods.service;

import cn.mabach.goods.entity.SkuStockEntity;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * sku的库存
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-26 01:03:48
 */
@Api(tags = "广告服务接口")
public interface SkuStockService   {

    @GetMapping("/listSkuStock")
    @ApiOperation(value = "分页查询")
    RS<PageResult<SkuStockEntity>> queryPage(@RequestParam(value = "keyword", required = false) String keyword,
                                             @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                             @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize);



    @GetMapping("/infoSkuStock")
    @ApiOperation(value = "根据id查找")
    RS<SkuStockEntity> getByIdE(@RequestParam("id") Long id);

    @PostMapping("/saveSkuStock")
    @ApiOperation(value = "保存")
    RS saveE(@RequestBody SkuStockEntity entity);

    @PostMapping("/updateSkuStock")
    @ApiOperation(value = "修改")
    RS updateByIdE(@RequestBody SkuStockEntity entity);



    /*
     * 根据商品编号及编码模糊搜索sku库存
     * */
    @ApiOperation("根据商品编号及编号模糊搜索sku库存")
    @GetMapping(value = "/pid")
    public RS<List<SkuStockEntity>> getList(@RequestParam("pid") Long pid, @RequestParam(value = "keyword",required = false) String keyword);

    @ApiOperation("批量更新库存信息")
    @PostMapping(value ="/update/pidSkuStockEntity")
    public RS update(@RequestParam("pid") Long pid, @RequestBody List<SkuStockEntity> skuStockList);
}

