package cn.mabach.business.service;


import cn.mabach.business.entity.FlashPromotionLogEntity;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.List;
/**
 * 限时购通知记录
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-31 21:38:07
 */
@Api(tags = "广告服务接口")
public interface FlashPromotionLogService   {

    @GetMapping("/listFlashPromotionLog")
    @ApiOperation(value = "分页查询")
    RS<PageResult<FlashPromotionLogEntity>> queryPage(@RequestParam(value = "keyword", required = false) String keyword,
                                                      @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                      @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize);



    @GetMapping("/infoFlashPromotionLog")
    @ApiOperation(value = "根据id查找")
    RS<FlashPromotionLogEntity> getByIdE(@RequestParam("id") Integer id);

    @PostMapping("/saveFlashPromotionLog")
    @ApiOperation(value = "保存")
    RS saveE(@RequestBody FlashPromotionLogEntity entity);

    @PostMapping("/updateFlashPromotionLog")
    @ApiOperation(value = "修改")
    RS updateByIdE(@RequestBody FlashPromotionLogEntity entity);

    @PostMapping("/deleteFlashPromotionLogs")
    @ApiOperation(value = "根据ID集合删除")
    RS removeByIdsE(@RequestParam("ids") List<Long> ids);

    @ApiOperation("单个删除")
    @RequestMapping(value = "/deleteFlashPromotionLogByid", method = RequestMethod.POST)
    public RS removeByIdE(@RequestParam("id") Long id) ;
}

