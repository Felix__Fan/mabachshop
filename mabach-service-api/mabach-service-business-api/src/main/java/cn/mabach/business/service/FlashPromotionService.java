package cn.mabach.business.service;


import cn.mabach.business.entity.FlashPromotionEntity;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.List;
/**
 * 限时购表
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-31 21:38:07
 */
@Api(tags = "FlashPromotionService", description = "限时购活动管理")
public interface FlashPromotionService   {

    @GetMapping("/listFlashPromotion")
    @ApiOperation(value = "分页查询")
    RS<PageResult<FlashPromotionEntity>> queryPage(@RequestParam(value = "keyword", required = false) String keyword,
                                                   @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                   @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
                                                  );



    @GetMapping("/infoFlashPromotion")
    @ApiOperation(value = "根据id查找")
    RS<FlashPromotionEntity> getByIdE(@RequestParam("id") Long id);

    @PostMapping("/saveFlashPromotions")
    @ApiOperation(value = "保存")
    RS saveE(@RequestBody FlashPromotionEntity entity);

    @PostMapping("/updateFlashPromotion")
    @ApiOperation(value = "修改")
    RS updateByIdE(@RequestBody FlashPromotionEntity entity);

    @PostMapping("/deleteFlashPromotions")
    @ApiOperation(value = "根据ID集合删除")
    RS removeByIdsE(@RequestParam("ids") List<Long> ids);

    @ApiOperation("单个删除")
    @RequestMapping(value = "/deleteFlashPromotionByid", method = RequestMethod.POST)
    public RS removeByIdE(@RequestParam("id") Long id) ;

    @ApiOperation("修改上下线状态")
    @RequestMapping(value = "/updateFlashPromotionStatusByid", method = RequestMethod.POST)
    public RS update(@RequestParam("id") Long id, @RequestParam("status") Integer status) ;
}

