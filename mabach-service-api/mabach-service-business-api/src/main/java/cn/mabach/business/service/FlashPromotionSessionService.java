package cn.mabach.business.service;


import cn.mabach.business.entity.FlashPromotionSessionEntity;
import cn.mabach.business.dto.FlashPromotionSessionDetail;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.List;
/**
 * 限时购场次表
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-31 21:38:07
 */
@Api(tags = "FlashPromotionSessionService", description = "限时购场次管理")
public interface FlashPromotionSessionService   {

    @GetMapping("/listFlashPromotionSession")
    @ApiOperation(value = "分页查询")
    RS<PageResult<FlashPromotionSessionEntity>> queryPage(@RequestParam(value = "keyword", required = false) String keyword,
                                                          @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                          @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize);



    @GetMapping("/infoFlashPromotionSession")
    @ApiOperation(value = "根据id查找")
    RS<FlashPromotionSessionEntity> getByIdE(@RequestParam("id") Long id);

    @PostMapping("/saveFlashPromotionSession")
    @ApiOperation(value = "保存")
    RS saveE(@RequestBody FlashPromotionSessionEntity entity);

    @PostMapping("/updateFlashPromotionSession")
    @ApiOperation(value = "修改")
    RS updateByIdE(@RequestBody FlashPromotionSessionEntity entity);

    @PostMapping("/deleteFlashPromotionSessions")
    @ApiOperation(value = "根据ID集合删除")
    RS removeByIdsE(@RequestParam("ids") List<Long> ids);

    @ApiOperation("单个删除")
    @RequestMapping(value = "/deleteFlashPromotionSessionByid", method = RequestMethod.POST)
    public RS removeByIdE(@RequestParam("id") Long id) ;

    @ApiOperation("修改启用状态")
    @RequestMapping(value = "/updateFlashPromotionSessionStatusByid", method = RequestMethod.POST)
    public RS updateStatus(@RequestParam("id") Long id, @RequestParam("status") Integer status) ;

    @ApiOperation("获取全部场次")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public RS<List<FlashPromotionSessionEntity>> listAll() ;

    @ApiOperation("获取全部可选场次及其数量")
    @RequestMapping(value = "/selectList", method = RequestMethod.GET)
    public RS<List<FlashPromotionSessionDetail>> selectList(@RequestParam("flashPromotionId") Long flashPromotionId) ;

}

