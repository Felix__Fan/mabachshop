

package cn.mabach.exception;



import cn.mabach.result.RS;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 异常处理器
 *
 * @author Mark sunlightcs@gmail.com
 */
@ControllerAdvice
public class RRExceptionHandler {
	private Logger logger = LoggerFactory.getLogger(getClass());




	/**
	 * 处理自定义异常
	 */
	@ExceptionHandler(RRException.class)
	@ResponseBody
	public RS exceptionHandler(HttpServletRequest request, RRException e){
		logger.info("RuntimeException  开始打印日志");
		logger.info("URL : {}, error : {}", request.getRequestURL(),e.getMessage(), e);
//		asyncMethod.senErrorLog(request,e);
		return RS.error(e.getCode(),e.getMessage());
	}

	@ExceptionHandler(RuntimeException.class)
	@ResponseBody
	public RS RuntimeException(HttpServletRequest request, RuntimeException e){
		logger.info("RuntimeException  开始打印日志");
		logger.info("URL : {}, error : {}", request.getRequestURL(),e.getMessage(), e);
//		asyncMethod.senErrorLog(request,e);
		return RS.error(e.getMessage());
	}


	@ExceptionHandler(NoHandlerFoundException.class)
	@ResponseBody
	public RS handlerNoFoundException(Exception e, HttpServletRequest request) {

		logger.error("URL : {}, error : {}", request.getRequestURL(), e.getMessage(),e);
//		asyncMethod.senErrorLog(request,e); //发送日志给kafka
		return RS.error(404, "路径不存在，请检查路径是否正确");
	}

	@ExceptionHandler(DuplicateKeyException.class)
	@ResponseBody
	public RS handleDuplicateKeyException(DuplicateKeyException e, HttpServletRequest request){

		logger.error("URL : {}, error : {}", request.getRequestURL(), e.getMessage(),e);
		//发送日志给kafka
//		asyncMethod.senErrorLog(request,e);
		return RS.error("数据库中已存在该记录");
	}

	@ExceptionHandler(DataIntegrityViolationException.class)
	@ResponseBody
	public RS handleDataIntegrityViolationException(DataIntegrityViolationException e, HttpServletRequest request) {
//		logger.error(e.getMessage(), e);
		logger.error("URL : {}, error : {}", request.getRequestURL(), e.getMessage(),e);
		//发送日志给kafka
//		asyncMethod.senErrorLog(request,e);
		return RS.error("字段太长,超出数据库字段的长度");
	}




	@ExceptionHandler(MaxUploadSizeExceededException.class)
	@ResponseBody
	public RS handleMaxUploadSizeExceededException(MaxUploadSizeExceededException e, HttpServletRequest request) {
		logger.error(e.getMessage(), e);
		//发送日志给kafka
//		asyncMethod.senErrorLog(request,e);
		return RS.error("文件大小超出10MB限制, 请压缩或降低文件质量! ");
	}


	@ExceptionHandler(ServletRequestBindingException.class)
	@ResponseBody
	public RS ServletRequestBindingException(ServletRequestBindingException e, HttpServletRequest request) {
		logger.error(e.getMessage(), e);
		//发送日志给kafka
//		asyncMethod.senErrorLog(request,e);
		return RS.error("请求参数错误: "+e.getMessage());
	}






	@ExceptionHandler(Exception.class)
	@ResponseBody
	public RS handleException(Exception e,HttpServletRequest request){
		logger.info("操作失败Exception  开始打印日志");
		logger.error(e.getMessage(), e);
		//发送日志给kafka
//		asyncMethod.senErrorLog(request,e);

		return RS.error("发生错误："+e.getMessage());
	}




}
