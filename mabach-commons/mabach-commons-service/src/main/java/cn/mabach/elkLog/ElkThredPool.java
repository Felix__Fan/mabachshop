//package cn.mabach.elkLog;
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.scheduling.annotation.EnableAsync;
//import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
//
//import java.util.concurrent.Executor;
//
///**
// * @Author: ming
// * @Date: 2020/2/11 0011 下午 11:51
// */
//@EnableAsync
//@Configuration
//public class ElkThredPool {
//    @Bean("Log")
//    public Executor taskExecutor() {
//        ThreadPoolTaskExecutor pool = new ThreadPoolTaskExecutor();
//        pool.setCorePoolSize(2); //核心线程数
//        pool.setQueueCapacity(4); //队列数
//        pool.setMaxPoolSize(2); //最大线程数
//        pool.setWaitForTasksToCompleteOnShutdown(true);
//        pool.setThreadNamePrefix("elk_log");
////        pool.setAllowCoreThreadTimeOut(true); //允许核心线程销毁
//        return pool;
//    }
//
//}
