package cn.mabach.order.service.impl;



import cn.mabach.order.dao.OrderReturnApplyDao;
import cn.mabach.order.dto.OrderReturnApplyResult;
import cn.mabach.order.entity.CompanyAddressEntity;
import cn.mabach.order.entity.OrderReturnApplyEntity;
import cn.mabach.order.service.CompanyAddressService;
import cn.mabach.order.service.OrderReturnApplyService;
import cn.mabach.order.vo.UpdateStatusParam;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import cn.mabach.utils.BeanUtilsMabach;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

@RestController
public class OrderReturnApplyServiceImpl extends ServiceImpl<OrderReturnApplyDao, OrderReturnApplyEntity> implements OrderReturnApplyService {


    @Autowired
    private CompanyAddressService companyAddressService;
    /*
     *分页查询
     * */
    @Override
    public RS<PageResult<OrderReturnApplyEntity>> queryPage(@RequestParam(value = "id",required = false) Long id,
                                                            @RequestParam(value = "receiverKeyword",required = false) String receiverKeyword,
                                                            @RequestParam(value = "status",required = false) Integer status,
                                                            @RequestParam(value = "createTime",required = false)  Date createTime,
                                                            @RequestParam(value = "handleMan",required = false) String handleMan,
                                                            @RequestParam(value = "handleTime",required = false)  Date handleTime,
                                                            @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                            @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {


        IPage<OrderReturnApplyEntity> page = this.page(
                new Page<OrderReturnApplyEntity>(pageNum,pageSize),
                new QueryWrapper<OrderReturnApplyEntity>().eq(id!=null,"id",id)
                        .like(!StringUtils.isEmpty(receiverKeyword),"return_name",receiverKeyword).or()
                .eq(!StringUtils.isEmpty(receiverKeyword),"return_phone",receiverKeyword)
                .eq(status!=null,"status",status)
                .eq(!StringUtils.isEmpty(createTime),"create_time",createTime)
                .like(!StringUtils.isEmpty(handleMan),"handle_man",handleMan)
                .like(!StringUtils.isEmpty(handleTime),"handle_time",handleTime)
        );

        return RS.ok(new PageResult(page));
    }



    /*
     *根据id查询
     * */
    @Override
    public RS<OrderReturnApplyResult> getByIdE(@RequestParam("id") Long id) {
        OrderReturnApplyEntity res = this.getById(id);
        RS<CompanyAddressEntity> byIdE = companyAddressService.getByIdE(res.getCompanyAddressId());
        OrderReturnApplyResult dto = BeanUtilsMabach.doToDto(res, OrderReturnApplyResult.class);
        dto.setCompanyAddressEntity(byIdE.getData());

        return RS.ok(dto);
    }

    @Override
    public RS saveE(@RequestBody  OrderReturnApplyEntity entity) {
        boolean b = this.save(entity);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    /*
     *根据id更改
     * */
    @Override
    public RS updateByIdE(@RequestBody OrderReturnApplyEntity entity) {
        boolean b = this.updateById(entity);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    /*
     *根据id集合删除
     * */
    @Override
    @Transactional
    public RS removeByIdsE(@RequestParam("ids") List<Long> ids) {
        boolean b = this.remove(new QueryWrapper<OrderReturnApplyEntity>().in("id", ids)
                .eq("status", 3));
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    /*
     *根据id删除
     * */
    public RS removeByIdE(@RequestParam("id") Long id){
        boolean b = this.removeById(id);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    @Override
    public RS updateStatus(@RequestBody UpdateStatusParam statusParam) {
        Integer status = statusParam.getStatus();
        OrderReturnApplyEntity returnApply = new OrderReturnApplyEntity();
        if (status==1){
            //确认退货
            returnApply.setId(statusParam.getId());
            returnApply.setStatus(1);
            returnApply.setReturnAmount(statusParam.getReturnAmount());
            returnApply.setCompanyAddressId(statusParam.getCompanyAddressId());
            returnApply.setHandleTime(new Date());
            returnApply.setHandleMan(statusParam.getHandleMan());
            returnApply.setHandleNote(statusParam.getHandleNote());
        }

        else if (status==2){
            //完成退货
            returnApply.setId(statusParam.getId());
            returnApply.setStatus(2);
            returnApply.setReceiveTime(new Date());
            returnApply.setReceiveMan(statusParam.getReceiveMan());
            returnApply.setReceiveNote(statusParam.getReceiveNote());
        }
       else if (status==3){
        //拒绝退货
            returnApply.setId(statusParam.getId());
            returnApply.setStatus(3);
            returnApply.setHandleTime(new Date());
            returnApply.setHandleMan(statusParam.getHandleMan());
            returnApply.setHandleNote(statusParam.getHandleNote());
        }else {
           return RS.error("退货状态不正确");
        }
        boolean b = this.updateById(returnApply);

       if (!b){
           return RS.error();
       }

        return RS.ok();
    }

}