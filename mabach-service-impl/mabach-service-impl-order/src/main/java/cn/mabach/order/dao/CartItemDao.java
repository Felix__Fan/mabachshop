package cn.mabach.order.dao;


import cn.mabach.order.entity.CartItemEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;


/**
 * 购物车表
 * 
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2020-01-02 23:46:11
 */
@Mapper
public interface CartItemDao extends BaseMapper<CartItemEntity> {
	
}
