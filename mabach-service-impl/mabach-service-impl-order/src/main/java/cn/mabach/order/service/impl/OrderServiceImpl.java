package cn.mabach.order.service.impl;



import cn.mabach.order.dao.OrderDao;
import cn.mabach.order.dto.OrderDeliveryParam;
import cn.mabach.order.dto.OrderDetail;
import cn.mabach.order.entity.OrderEntity;
import cn.mabach.order.entity.OrderItemEntity;
import cn.mabach.order.entity.OrderOperateHistoryEntity;
import cn.mabach.order.service.OrderItemService;
import cn.mabach.order.service.OrderOperateHistoryService;
import cn.mabach.order.service.OrderService;

import cn.mabach.order.vo.MoneyInfoParam;
import cn.mabach.order.vo.ReceiverInfoParam;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import cn.mabach.utils.BeanUtilsMabach;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

@RestController
public class OrderServiceImpl extends ServiceImpl<OrderDao, OrderEntity> implements OrderService {

    @Autowired
    private OrderItemService orderItemService;
    @Autowired
    private OrderOperateHistoryService orderOperateHistoryService;
    /*
     *分页查询
     * */
    @Override
    public RS<PageResult<OrderEntity>> queryPage(@RequestParam(value = "orderSn",required = false) String orderSn,
                                                 @RequestParam(value = "receiverKeyword",required = false) String receiverKeyword,
                                                 @RequestParam(value = "status",required = false) Integer status,
                                                 @RequestParam(value = "orderType",required = false) Integer orderType,
                                                 @RequestParam(value = "sourceType",required = false) Integer sourceType,
                                                 @RequestParam(value = "createTime",required = false) Date createTime,
                                                 @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                 @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {

        IPage<OrderEntity> page = this.page(
                new Page<OrderEntity>(pageNum,pageSize),
                new QueryWrapper<OrderEntity>().eq(!StringUtils.isEmpty(orderSn),"order_sn",orderSn)
                        .eq(!StringUtils.isEmpty(receiverKeyword),"receiver_name",receiverKeyword).or()
                        .eq(!StringUtils.isEmpty(receiverKeyword),"receiver_phone",receiverKeyword)
                        .eq(status!=null,"status",status)
                        .eq(orderType!=null,"order_type",orderType)
                        .eq(sourceType!=null,"source_type",sourceType)
                        .like(!StringUtils.isEmpty(createTime),"create_time",createTime)
        );

        return RS.ok(new PageResult(page));

    }








    /*
     *根据id查询
     * */
    @Override
    public RS<OrderDetail> getByIdE(@RequestParam("id") Long id) {
        OrderEntity res = this.getById(id);
        OrderDetail detail = BeanUtilsMabach.doToDto(res, OrderDetail.class);
        List<OrderItemEntity> orderByIdList = orderItemService.getOrderById(id);
        List<OrderOperateHistoryEntity> orderOperateHistoryEntityByOrderId = orderOperateHistoryService.getOrderOperateHistoryEntityByOrderId(id);
        detail.setOrderItemList(orderByIdList);
        detail.setHistoryList(orderOperateHistoryEntityByOrderId);
        return RS.ok(detail);
    }

    @Override
    public RS saveE(@RequestBody  OrderEntity entity) {
        boolean b = this.save(entity);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    /*
     *根据id更改
     * */
    @Override
    public RS updateByIdE(@RequestBody OrderEntity entity) {
        boolean b = this.updateById(entity);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    /*
     *根据id集合删除
     * */
    @Override
    @Transactional
    public RS removeByIdsE(@RequestParam("ids") List<Long> ids) {
        boolean b = this.removeByIds(ids);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    /*
     *根据id删除
     * */
    public RS removeByIdE(@RequestParam("id") Long id){
        boolean b = this.removeById(id);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    @Override
    public RS delivery(@RequestBody OrderDeliveryParam deliveryParam) {


            OrderEntity orderEntity = new OrderEntity();
            orderEntity.setId(deliveryParam.getOrderId());
            orderEntity.setDeliveryCompany(deliveryParam.getDeliveryCompany());
            orderEntity.setDeliverySn(orderEntity.getOrderSn());
            boolean b = this.updateById(orderEntity);
            if (!b){
                return RS.error();
            }





        return RS.ok();
    }

    @Override
    public RS close(Long id) {
        OrderEntity entity = new OrderEntity();
        entity.setId(id);
        entity.setStatus(4);
        boolean b = this.updateById(entity);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    @Override
    public RS updateReceiverInfo(@RequestBody ReceiverInfoParam receiverInfoParam) {

        OrderEntity order = new OrderEntity();
        order.setId(receiverInfoParam.getOrderId());
        order.setReceiverName(receiverInfoParam.getReceiverName());
        order.setReceiverPhone(receiverInfoParam.getReceiverPhone());
        order.setReceiverPostCode(receiverInfoParam.getReceiverPostCode());
        order.setReceiverDetailAddress(receiverInfoParam.getReceiverDetailAddress());
        order.setReceiverProvince(receiverInfoParam.getReceiverProvince());
        order.setReceiverCity(receiverInfoParam.getReceiverCity());
        order.setReceiverRegion(receiverInfoParam.getReceiverRegion());
        order.setModifyTime(new Date());
        boolean b = this.updateById(order);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    @Override
    public RS updateMoneyInfo(MoneyInfoParam moneyInfoParam) {
        OrderEntity order = new OrderEntity();
        order.setId(moneyInfoParam.getOrderId());
        order.setFreightAmount(moneyInfoParam.getFreightAmount());
        order.setDiscountAmount(moneyInfoParam.getDiscountAmount());
        order.setModifyTime(new Date());
        boolean b = this.updateById(order);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    @Override
    public RS updateNote(@RequestParam("id") Long id,
                         @RequestParam("note") String note) {
        OrderEntity order = new OrderEntity();
        order.setId(id);
        order.setNote(note);
        order.setModifyTime(new Date());
        boolean b = this.updateById(order);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

}