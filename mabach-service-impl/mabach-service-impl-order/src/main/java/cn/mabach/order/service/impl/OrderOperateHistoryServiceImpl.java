package cn.mabach.order.service.impl;



import cn.mabach.order.dao.OrderOperateHistoryDao;
import cn.mabach.order.entity.OrderOperateHistoryEntity;
import cn.mabach.order.service.OrderOperateHistoryService;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
public class OrderOperateHistoryServiceImpl extends ServiceImpl<OrderOperateHistoryDao, OrderOperateHistoryEntity> implements OrderOperateHistoryService {

    /*
     *分页查询
     * */
    @Override
    public RS<PageResult<OrderOperateHistoryEntity>> queryPage(@RequestParam(value = "keyword", required = false) String keyword,
                                                               @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                               @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {


        IPage<OrderOperateHistoryEntity> page = this.page(
                new Page<OrderOperateHistoryEntity>(pageNum,pageSize),
                new QueryWrapper<OrderOperateHistoryEntity>().like(!StringUtils.isEmpty(keyword),"name",keyword)
        );

        return RS.ok(new PageResult(page));
    }



    /*
     *根据id查询
     * */
    @Override
    public RS<OrderOperateHistoryEntity> getByIdE(@RequestParam("id") Long id) {
            OrderOperateHistoryEntity res = this.getById(id);

        return RS.ok(res);
    }

    @Override
    public RS saveE(@RequestBody  OrderOperateHistoryEntity entity) {
        boolean b = this.save(entity);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    /*
     *根据id更改
     * */
    @Override
    public RS updateByIdE(@RequestBody OrderOperateHistoryEntity entity) {
        boolean b = this.updateById(entity);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    /*
     *根据id集合删除
     * */
    @Override
    @Transactional
    public RS removeByIdsE(@RequestParam("ids") List<Long> ids) {
        boolean b = this.removeByIds(ids);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    /*
     *根据id删除
     * */
    public RS removeByIdE(@RequestParam("id") Long id){
        boolean b = this.removeById(id);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    @Override
    public List<OrderOperateHistoryEntity> getOrderOperateHistoryEntityByOrderId(@RequestParam("id") Long id) {
        return  this.list(new QueryWrapper<OrderOperateHistoryEntity>().eq("order_id",id));

    }

}