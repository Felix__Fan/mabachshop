package cn.mabach.goods.dao;


import cn.mabach.goods.entity.BrandEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;


/**
 * 品牌表
 * 
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-25 22:24:35
 */
@Mapper
public interface BrandDao extends BaseMapper<BrandEntity> {
	
}
