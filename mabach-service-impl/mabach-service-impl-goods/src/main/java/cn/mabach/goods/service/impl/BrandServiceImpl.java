package cn.mabach.goods.service.impl;


import cn.mabach.goods.dao.BrandDao;
import cn.mabach.goods.entity.BrandEntity;
import cn.mabach.goods.service.BrandService;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Slf4j
public class BrandServiceImpl extends ServiceImpl<BrandDao, BrandEntity> implements BrandService {

    @Override
    public RS<PageResult<BrandEntity>> queryPage(@RequestParam(value = "keyword", required = false) String keyword,
                                                 @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                 @RequestParam(value = "pageSize", defaultValue = "5") Integer pageSize) {

        IPage<BrandEntity> page = this.page(
                new Page<BrandEntity>(pageNum,pageSize),
                new QueryWrapper<BrandEntity>().like(!StringUtils.isEmpty(keyword),"name",keyword)
        );

        log.info("BrandServiceImpl:{}",Thread.currentThread().getName());
        return RS.ok(new PageResult(page));
    }


    @Override
    public RS<List<BrandEntity>> queryAll() {
        List<BrandEntity> list = this.list();

        return RS.ok(list);
    }


    @Override
    public RS<BrandEntity> getByIdE(@RequestParam("id") Long id) {
            BrandEntity res = this.getById(id);

        return RS.ok(res);
    }

    @Override
    public RS saveE(@RequestBody  BrandEntity entity) {
        boolean b = this.save(entity);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    @Override
    public RS updateByIdE(@RequestBody BrandEntity entity) {
        boolean b = this.updateById(entity);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }


    @Override
    public RS removeOneById(@RequestParam("id") Long id) {
        boolean b = this.removeById(id);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    @Override
    public RS removeByIdsE(@RequestBody List<Long> ids) {
        boolean b = this.removeByIds(ids);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    /*
    *批量更新显示状态
    * */
    @Override
    @Transactional
    public RS updateShowStatus(@RequestParam("ids") List<Long> ids,
                               @RequestParam("showStatus") Integer showStatus) {
        for (Long id : ids) {
            BrandEntity brandEntity = this.getById(id);
            brandEntity.setShowStatus(showStatus);
            boolean b = this.updateById(brandEntity);
            if (!b){
                return RS.error();
            }
        }
        return RS.ok();
    }

    /*
    * 批量更新厂家制造商状态
    * */
    @Override
    public RS updateFactoryStatus(@RequestParam("ids") List<Long> ids,
                                  @RequestParam("factoryStatus") Integer factoryStatus) {
        for (Long id : ids) {
            BrandEntity brandEntity = this.getById(id);
            brandEntity.setFactoryStatus(factoryStatus);
            boolean b = this.updateById(brandEntity);
            if (!b){
                return RS.error();
            }
        }

        return RS.ok();
    }

}