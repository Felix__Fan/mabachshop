package cn.mabach.goods.dao;


import cn.mabach.goods.entity.MemberPriceEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;


/**
 * 商品会员价格表
 * 
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-25 22:24:34
 */
@Mapper
public interface MemberPriceDao extends BaseMapper<MemberPriceEntity> {
	
}
