package cn.mabach.goods.dao;


import cn.mabach.goods.entity.ProductVertifyRecordEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;


/**
 * 商品审核记录
 * 
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-25 22:23:55
 */
@Mapper
public interface ProductVertifyRecordDao extends BaseMapper<ProductVertifyRecordEntity> {
	
}
