package cn.mabach.goods.service.impl;


import cn.mabach.goods.dao.ProductCategoryDao;
import cn.mabach.goods.entity.ProductCategoryEntity;
import cn.mabach.goods.service.ProductCategoryService;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@Slf4j
public class ProductCategoryServiceImpl extends ServiceImpl<ProductCategoryDao, ProductCategoryEntity> implements ProductCategoryService {

    @Override
    public RS<PageResult<ProductCategoryEntity>> queryPage(@RequestParam(value = "parentId" ) Long parentId,
                                                           @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                           @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {

        IPage<ProductCategoryEntity> page = this.page(
                new Page<ProductCategoryEntity>(pageNum,pageSize),
                new QueryWrapper<ProductCategoryEntity>().eq(parentId!=null,"parent_id",parentId)
        );

        log.info("ProductCategoryServiceImpl:{}",Thread.currentThread().getName());

        return RS.ok(new PageResult(page));
    }





    @Override
    public RS<ProductCategoryEntity> getByIdE(@RequestParam("id") Long id) {
            ProductCategoryEntity res = this.getById(id);

        return RS.ok(res);
    }

    @Override
    public RS saveE(@RequestBody  ProductCategoryEntity entity) {
        boolean b = this.save(entity);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    @Override
    public RS updateByIdE(@RequestBody ProductCategoryEntity entity) {
        boolean b = this.updateById(entity);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }


    @Override
    public RS removeOneById(@RequestParam("id") Long id) {
        boolean b = this.removeById(id);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    @Override
    public RS removeByIdsE(@RequestBody List<Long> ids) {
        boolean b = this.removeByIds(ids);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    @Override
    @Transactional
    public RS updateNavStatus(@RequestParam("ids") Long ids, @RequestParam("navStatus") Integer navStatus) {


            ProductCategoryEntity entity = new ProductCategoryEntity();
            entity.setId(ids);
            entity.setNavStatus(navStatus);
            boolean b = this.updateById(entity);
            if (!b){
                return RS.error();
            }

        return RS.ok();
    }

    @Override
    @Transactional
    public RS updateShowStatus(@RequestParam("ids") Long ids, @RequestParam("showStatus") Integer showStatus) {



            ProductCategoryEntity entity = new ProductCategoryEntity();
            entity.setId(ids);
            entity.setShowStatus(showStatus);
            boolean b = this.updateById(entity);
            if (!b){
                return RS.error();
            }

        return RS.ok();


    }

    @Override
    public RS<List<Map>> listWithChildren() {
        List<ProductCategoryEntity> list = this.list();
        List<Map> tree = findTree(list, 0L);
        return RS.ok(tree);
    }

    public List<Map> findTree(List<ProductCategoryEntity> list, Long parentId){
        ArrayList<Map> arrayList = new ArrayList<>();

        for (ProductCategoryEntity productCategoryEntity : list) {
            HashMap<String, Object> hashMap = new HashMap<>();
            if (productCategoryEntity.getParentId().equals(parentId)){
                hashMap.put("id",productCategoryEntity.getId());
                hashMap.put("name",productCategoryEntity.getName());
                hashMap.put("children",findTree(list,productCategoryEntity.getId()));
                arrayList.add(hashMap);
            }
        }

        return arrayList;
    }

}