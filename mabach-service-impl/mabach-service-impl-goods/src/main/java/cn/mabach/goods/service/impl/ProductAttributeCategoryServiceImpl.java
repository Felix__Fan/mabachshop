package cn.mabach.goods.service.impl;


import cn.mabach.goods.dao.ProductAttributeCategoryDao;
import cn.mabach.goods.dao.ProductAttributeDao;
import cn.mabach.goods.entity.ProductAttributeCategoryEntity;
import cn.mabach.goods.dto.ProductAttributeCategoryItem;
import cn.mabach.goods.entity.ProductAttributeEntity;
import cn.mabach.goods.service.ProductAttributeCategoryService;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import cn.mabach.utils.BeanUtilsMabach;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@Slf4j
public class ProductAttributeCategoryServiceImpl extends ServiceImpl<ProductAttributeCategoryDao, ProductAttributeCategoryEntity> implements ProductAttributeCategoryService {

    @Autowired
    private ProductAttributeDao productAttributeDao;
    @Override
    public RS<PageResult<ProductAttributeCategoryEntity>> queryPage(
                                                                    @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                                    @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {

        IPage<ProductAttributeCategoryEntity> page = this.page(
                new Page<ProductAttributeCategoryEntity>(pageNum,pageSize),
                new QueryWrapper<ProductAttributeCategoryEntity>()

        );
        log.info("ProductAttributeCategoryServiceImpl:{}",Thread.currentThread().getName());
        return RS.ok(new PageResult(page));
    }





    @Override
    public RS<ProductAttributeCategoryEntity> getByIdE(@RequestParam("id") Long id) {
            ProductAttributeCategoryEntity res = this.getById(id);

        return RS.ok(res);
    }

    @Override
    public RS saveE(@RequestBody  ProductAttributeCategoryEntity entity) {
        boolean b = this.save(entity);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    @Override
    public RS updateByIdE(@RequestBody ProductAttributeCategoryEntity entity) {
        boolean b = this.updateById(entity);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }


    @Override
    public RS removeOneById(@RequestParam("id") Long id) {
        boolean b = this.removeById(id);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    @Override
    public RS removeByIdsE(@RequestBody List<Long> ids) {
        boolean b = this.removeByIds(ids);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    @Override
    public RS<List<ProductAttributeCategoryItem>> getListWithAttr() {

        System.out.println("执行了嘻嘻嘻嘻嘻");

        List<ProductAttributeCategoryEntity> list = this.list();
        ArrayList<ProductAttributeCategoryItem> items = new ArrayList<>();

        for (ProductAttributeCategoryEntity productAttributeCategoryEntity : list) {
            ProductAttributeCategoryItem productAttributeCategoryItem = BeanUtilsMabach.doToDto(productAttributeCategoryEntity, ProductAttributeCategoryItem.class);

            List<ProductAttributeEntity> product_attribute_category_id = productAttributeDao.selectList(new QueryWrapper<ProductAttributeEntity>().
                    eq("product_attribute_category_id" , productAttributeCategoryEntity.getId()));

           productAttributeCategoryItem.setProductAttributeList(product_attribute_category_id);
           items.add(productAttributeCategoryItem);

        }
        return RS.ok(items);

    }

}