package cn.mabach.goods.service.impl;


import cn.mabach.goods.dao.ProductAttributeValueDao;
import cn.mabach.goods.entity.ProductAttributeValueEntity;
import cn.mabach.goods.service.ProductAttributeValueService;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Slf4j
public class ProductAttributeValueServiceImpl extends ServiceImpl<ProductAttributeValueDao, ProductAttributeValueEntity> implements ProductAttributeValueService {

    @Override
    public RS<PageResult<ProductAttributeValueEntity>> queryPage(@RequestParam(value = "keyword", required = false) String keyword,
                                                                 @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                                 @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {
        if (pageSize>20){
            return RS.error("ProductAttributeValueServiceImpl非法参数");
        }
        IPage<ProductAttributeValueEntity> page = this.page(
                new Page<ProductAttributeValueEntity>(pageNum,pageSize),
                new QueryWrapper<ProductAttributeValueEntity>().like("name",keyword)
        );
        log.info("ProductAttributeValueServiceImpl:{}",Thread.currentThread().getName());
        return RS.ok(new PageResult(page));
    }





    @Override
    public RS<ProductAttributeValueEntity> getByIdE(@RequestParam("id") Long id) {
            ProductAttributeValueEntity res = this.getById(id);

        return RS.ok(res);
    }

    @Override
    public RS saveE(@RequestBody  ProductAttributeValueEntity entity) {
        boolean b = this.save(entity);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    @Override
    public RS updateByIdE(@RequestBody ProductAttributeValueEntity entity) {
        boolean b = this.updateById(entity);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }


    @Override
    public RS removeOneById(@RequestParam("id") Long id) {
        boolean b = this.removeById(id);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    @Override
    public RS removeByIdsE(@RequestBody List<Long> ids) {
        boolean b = this.removeByIds(ids);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

}