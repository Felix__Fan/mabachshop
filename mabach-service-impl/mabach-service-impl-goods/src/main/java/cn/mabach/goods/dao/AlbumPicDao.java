package cn.mabach.goods.dao;


import cn.mabach.goods.entity.AlbumPicEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;


/**
 * 画册图片表
 * 
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-25 22:24:35
 */
@Mapper
public interface AlbumPicDao extends BaseMapper<AlbumPicEntity> {
	
}
