package cn.mabach.member.dao;


import cn.mabach.member.entity.MemberStatisticsInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;


/**
 * 会员统计信息
 * 
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2020-01-04 17:30:47
 */
@Mapper
public interface MemberStatisticsInfoDao extends BaseMapper<MemberStatisticsInfoEntity> {
	
}
