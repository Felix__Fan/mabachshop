package cn.mabach.member.service.impl;



import cn.mabach.member.dao.MemberStatisticsInfoDao;
import cn.mabach.member.entity.MemberStatisticsInfoEntity;
import cn.mabach.member.service.MemberStatisticsInfoService;
import cn.mabach.result.RS;
import cn.mabach.result.TableDataInfo;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.List;

@RestController
public class MemberStatisticsInfoServiceImpl extends ServiceImpl<MemberStatisticsInfoDao, MemberStatisticsInfoEntity> implements MemberStatisticsInfoService {

    /*
     *分页查询
     * */
    @Override
    public TableDataInfo queryPage(@RequestParam(value = "consumeAmount", required = false) BigDecimal consumeAmount,
                                   @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                   @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {


        IPage<MemberStatisticsInfoEntity> page = this.page(
                new Page<MemberStatisticsInfoEntity>(pageNum,pageSize),
                new QueryWrapper<MemberStatisticsInfoEntity>().eq(consumeAmount!=null,"consume_amount",consumeAmount)
        );

        return new TableDataInfo(page);
    }



    /*
     *根据id查询
     * */
    @Override
    public RS<MemberStatisticsInfoEntity> getByIdE(@RequestParam("id") Long id) {
            MemberStatisticsInfoEntity res = this.getById(id);

        return RS.ok(res);
    }

    /*
     *新增
     * */
    @Override
    public RS saveE(@RequestBody  MemberStatisticsInfoEntity entity) {
        boolean b = this.save(entity);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    /*
     *根据id更改
     * */
    @Override
    public RS updateByIdE(@RequestBody MemberStatisticsInfoEntity entity) {
        boolean b = this.updateById(entity);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    /*
     *根据id集合删除
     * */
    @Override
    @Transactional
    public RS removeByIdsE(@RequestParam("ids") List<Long> ids) {
        boolean b = this.removeByIds(ids);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }



}