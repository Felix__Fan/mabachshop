//package cn.mabach.member.config;
//
//import org.apache.commons.io.IOUtils;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.core.io.ClassPathResource;
//
//
//
//import java.io.IOException;
//
//
///**
// * @Author: ming
// * @Date: 2020/1/31 0031 下午 8:02
// * @Version: 1.0
// */
//@Configuration
//public class JwtDecode {
//
//
//    /***
//     * 定义JwtTokenStore
//     * @return
//     */
//    @Bean
//    public TokenStore tokenStore() {
//        return new JwtTokenStore(jwtAccessTokenConverter());
//    }
//
//    /***
//     * 读取公钥并封装进JwtAccessTokenConverter
//     * @return
//     */
//    @Bean
//    public JwtAccessTokenConverter jwtAccessTokenConverter() {
//        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
//        ClassPathResource resource = new ClassPathResource("public.key");
//        String publicKey = null;
//        try {
//            publicKey = IOUtils.toString(resource.getInputStream(), "UTF-8");
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        converter.setVerifierKey(publicKey);
//        return converter;
//    }
//
//
//}
