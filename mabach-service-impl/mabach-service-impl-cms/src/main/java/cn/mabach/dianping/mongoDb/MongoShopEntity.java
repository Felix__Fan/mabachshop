package cn.mabach.dianping.mongoDb;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-02-28 20:06:25
 */
@Data
@TableName("shop")
public class MongoShopEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer _id;
	/**
	 * 
	 */
	private Date createdTime;
	/**
	 * 
	 */
	private Date updatedTime;
	/**
	 * 
	 */
	private String name;
	/**
	 * 
	 */
	private BigDecimal remarkScore;
	/**
	 * 
	 */
	private Integer pricePerMan;
	/**
	 * 
	 */
	private BigDecimal latitude;
	/**
	 * 
	 */
	private BigDecimal longitude;
	/**
	 * 
	 */
	private Integer categoryId;
	/**
	 * 
	 */
	private String tags;
	/**
	 * 
	 */
	private String startTime;
	/**
	 * 
	 */
	private String endTime;
	/**
	 * 
	 */
	private String address;
	/**
	 * 
	 */
	private Integer sellerId;
	/**
	 * 
	 */
	private String logo;

	private String hot;

	private String category;


}
