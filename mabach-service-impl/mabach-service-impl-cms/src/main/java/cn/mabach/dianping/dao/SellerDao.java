package cn.mabach.dianping.dao;


import cn.mabach.dianping.entity.SellerEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-02-28 11:26:04
 */
@Mapper
public interface SellerDao extends BaseMapper<SellerEntity> {
	
}
