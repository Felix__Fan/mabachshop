package cn.mabach.dianping.config;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author: ming
 * @Date: 2020/3/1 0001 上午 11:04
 */
@Configuration
public class Esconfig {
    @Value("${elasticsearch.ip1}")
    private String hsot1;
    @Value("${elasticsearch.port1}")
    private String port1;


    @Bean
    public RestHighLevelClient restHighLevelClient(){
        RestClientBuilder builder = RestClient.builder(new HttpHost(hsot1, Integer.valueOf(port1), "http"));
        RestHighLevelClient restHighLevelClient = new RestHighLevelClient(builder);
        return restHighLevelClient;
    }
}
