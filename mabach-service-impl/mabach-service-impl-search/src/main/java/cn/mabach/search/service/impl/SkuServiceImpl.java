package cn.mabach.search.service.impl;




import cn.mabach.result.RS;
import cn.mabach.result.TableDataInfo;
import cn.mabach.search.dao.SkuDao;
import cn.mabach.search.entity.SkuEntity;
import cn.mabach.search.service.SkuService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
public class SkuServiceImpl extends ServiceImpl<SkuDao, SkuEntity> implements SkuService {

    /*
     *分页查询
     * */
    @Override
    public TableDataInfo queryPage(@RequestParam(value = "keyword", required = false) String keyword,
                                                        @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                        @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {


        IPage<SkuEntity> page = this.page(
                new Page<SkuEntity>(pageNum,pageSize),
                new QueryWrapper<SkuEntity>().like(!StringUtils.isEmpty(keyword),"name",keyword)
        );

        return new TableDataInfo(page);
    }



    /*
     *根据id查询
     * */
    @Override
    public RS<SkuEntity> getByIdE(@RequestParam("id") String id) {
            SkuEntity res = this.getById(id);

        return RS.ok(res);
    }

    @Override
    public RS<List<SkuEntity>> getall() {
        List<SkuEntity> list = this.list();
        return RS.ok(list);
    }

    /*
     *新增
     * */
    @Override
    public RS saveE(@RequestBody  SkuEntity entity) {
        boolean b = this.save(entity);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    /*
     *根据id更改
     * */
    @Override
    public RS updateByIdE(@RequestBody SkuEntity entity) {
        boolean b = this.updateById(entity);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    /*
     *根据id集合删除
     * */
    @Override
    @Transactional
    public RS removeByIdsE(@RequestParam("ids") List<Long> ids) {
        boolean b = this.removeByIds(ids);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }



}