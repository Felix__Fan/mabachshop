

package cn.mabach;


import com.spring4all.swagger.EnableSwagger2Doc;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;


@SpringBootApplication
@EnableEurekaClient
@EnableSwagger2Doc
@EnableCaching
@EnableFeignClients
@MapperScan(basePackages = "cn.mabach.search.dao")
public class SearchApp {

	public static void main(String[] args) {

		System.setProperty("es.set.netty.runtime.available.processors", "false");
		SpringApplication.run(SearchApp.class, args);
	}

}