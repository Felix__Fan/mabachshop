package cn.mabach.business.service.impl;



import cn.mabach.business.dao.CouponProductCategoryRelationDao;
import cn.mabach.business.entity.CouponProductCategoryRelationEntity;
import cn.mabach.business.service.CouponProductCategoryRelationService;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
public class CouponProductCategoryRelationServiceImpl extends ServiceImpl<CouponProductCategoryRelationDao, CouponProductCategoryRelationEntity> implements CouponProductCategoryRelationService {

    /*
     *分页查询
     * */
    @Override
    public RS<PageResult<CouponProductCategoryRelationEntity>> queryPage(@RequestParam(value = "keyword", required = false) String keyword,
                                                                         @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                                         @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {


        IPage<CouponProductCategoryRelationEntity> page = this.page(
                new Page<CouponProductCategoryRelationEntity>(pageNum,pageSize),
                new QueryWrapper<CouponProductCategoryRelationEntity>().like(!StringUtils.isEmpty(keyword),"name",keyword)
        );

        return RS.ok(new PageResult(page));
    }

    @Override
    public RS<List<CouponProductCategoryRelationEntity>> getByCouponId(Long id) {
        List<CouponProductCategoryRelationEntity> coupon_id = this.list(new QueryWrapper<CouponProductCategoryRelationEntity>().eq("coupon_id", id));


        return RS.ok(coupon_id);
    }


    /*
     *根据id查询
     * */
    @Override
    public RS<CouponProductCategoryRelationEntity> getByIdE(@RequestParam("id") Long id) {
            CouponProductCategoryRelationEntity res = this.getById(id);

        return RS.ok(res);
    }

    @Override
    public RS saveE(@RequestBody  CouponProductCategoryRelationEntity entity) {
        boolean b = this.save(entity);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    /*
     *根据id更改
     * */
    @Override
    public RS updateByIdE(@RequestBody CouponProductCategoryRelationEntity entity) {
        boolean b = this.updateById(entity);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    /*
     *根据id集合删除
     * */
    @Override
    @Transactional
    public RS removeByIdsE(@RequestParam("ids") List<Long> ids) {
        boolean b = this.removeByIds(ids);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    @Override
    public RS deleteByCouponId(Long id) {
        boolean b = this.remove(new QueryWrapper<CouponProductCategoryRelationEntity>().eq("coupon_id", id));
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }


}