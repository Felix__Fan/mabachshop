package cn.mabach.business.service.impl;



import cn.mabach.business.dao.FlashPromotionDao;
import cn.mabach.business.entity.FlashPromotionEntity;
import cn.mabach.business.service.FlashPromotionService;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

@RestController
public class FlashPromotionServiceImpl extends ServiceImpl<FlashPromotionDao, FlashPromotionEntity> implements FlashPromotionService {

    /*
     *分页查询
     * */
    @Override
    public RS<PageResult<FlashPromotionEntity>> queryPage(@RequestParam(value = "keyword", required = false) String keyword,
                                                          @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                          @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
                                                          ) {


        IPage<FlashPromotionEntity> page = this.page(
                new Page<FlashPromotionEntity>(pageNum,pageSize),
                new QueryWrapper<FlashPromotionEntity>().like(!StringUtils.isEmpty(keyword),"title",keyword)
        );


        return RS.ok(new PageResult<FlashPromotionEntity>(page));
    }



    /*
     *根据id查询
     * */
    @Override
    public RS<FlashPromotionEntity> getByIdE(@RequestParam("id") Long id) {
            FlashPromotionEntity res = this.getById(id);

        return RS.ok(res);
    }

    @Override
    public RS saveE(@RequestBody  FlashPromotionEntity entity) {
        entity.setCreateTime(new Date());

        boolean b = this.save(entity);
        if (!b){
            return RS.error("保存失败");
        }
        return RS.ok();
    }

    /*
     *根据id更改
     * */
    @Override
    public RS updateByIdE(@RequestBody FlashPromotionEntity entity) {
        boolean b = this.updateById(entity);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    /*
     *根据id集合删除
     * */
    @Override
    @Transactional
    public RS removeByIdsE(@RequestParam("ids") List<Long> ids) {
        boolean b = this.removeByIds(ids);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    /*
     *根据id删除
     * */
    public RS removeByIdE(@RequestParam("id") Long id){
        boolean b = this.removeById(id);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    @Override
    public RS update(@RequestParam("id") Long id, @RequestParam("status") Integer status) {
        FlashPromotionEntity flashPromotionEntity = new FlashPromotionEntity();
        flashPromotionEntity.setId(id);
        flashPromotionEntity.setStatus(status);
        boolean b = this.updateById(flashPromotionEntity);
        if (!b){
            return RS.error();
        }

        return RS.ok();
    }

}