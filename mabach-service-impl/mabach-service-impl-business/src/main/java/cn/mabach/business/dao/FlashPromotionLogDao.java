package cn.mabach.business.dao;


import cn.mabach.business.entity.FlashPromotionLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;


/**
 * 限时购通知记录
 * 
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-31 17:40:52
 */
@Mapper
public interface FlashPromotionLogDao extends BaseMapper<FlashPromotionLogEntity> {
	
}
