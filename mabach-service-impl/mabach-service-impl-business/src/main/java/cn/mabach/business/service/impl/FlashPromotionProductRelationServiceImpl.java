package cn.mabach.business.service.impl;



import cn.mabach.business.dto.FlashPromotionProduct;
import cn.mabach.business.entity.FlashPromotionProductRelationEntity;
import cn.mabach.business.service.FlashPromotionProductRelationService;
import cn.mabach.business.dao.FlashPromotionProductRelationDao;
import cn.mabach.business.feign.ProductServiceFeign;

import cn.mabach.goods.entity.ProductEntity;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import cn.mabach.utils.BeanUtilsMabach;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class FlashPromotionProductRelationServiceImpl extends ServiceImpl<FlashPromotionProductRelationDao, FlashPromotionProductRelationEntity> implements FlashPromotionProductRelationService {

    @Autowired
    private ProductServiceFeign productServiceFeign;
    /*
     *分页查询
     * */
    @Override
    public RS<PageResult<FlashPromotionProduct>> queryPage(@RequestParam(value = "flashPromotionId",required = false) Long flashPromotionId,
                                                           @RequestParam(value = "flashPromotionSessionId",required = false) Long flashPromotionSessionId,
                                                           @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                           @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {

//查询秒杀活动
        IPage<FlashPromotionProductRelationEntity> page = this.page(
                new Page<FlashPromotionProductRelationEntity>(pageNum,pageSize),
                new QueryWrapper<FlashPromotionProductRelationEntity>().eq(flashPromotionId!=null,"flash_promotion_id",flashPromotionId)
                .eq(flashPromotionSessionId!=null,"flash_promotion_session_id",flashPromotionSessionId)
                .orderByAsc("sort")
        );
//        根据秒杀活动中的商品ID查询对应的商品并装入FlashPromotionProduct中
        ArrayList<FlashPromotionProduct> arrayList = new ArrayList<>();
        for (FlashPromotionProductRelationEntity entity : page.getRecords()) {
            RS<ProductEntity> oneById = productServiceFeign.getOneById(entity.getProductId());
            FlashPromotionProduct flashPromotionProduct = BeanUtilsMabach.doToDto(entity, FlashPromotionProduct.class);
            flashPromotionProduct.setProduct(oneById.getData());
            arrayList.add(flashPromotionProduct);
        }
//封装pageResult
        PageResult<FlashPromotionProduct> pageResult = new PageResult<>();
        pageResult.setList(arrayList);
        pageResult.setTotal(page.getTotal());
        pageResult.setTotalPage(page.getPages());
        pageResult.setPageSize(page.getSize());
        pageResult.setPageNum(page.getCurrent());
        return RS.ok(pageResult);
    }



    /*
     *根据id查询
     * */
    @Override
    public RS<FlashPromotionProductRelationEntity> getByIdE(@RequestParam("id") Long id) {
            FlashPromotionProductRelationEntity res = this.getById(id);

        return RS.ok(res);
    }

    @Override
    public RS saveE(@RequestBody  FlashPromotionProductRelationEntity entity) {
        boolean b = this.save(entity);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    @Override
    public RS saveS(@RequestBody List<FlashPromotionProductRelationEntity> list) {
        for (FlashPromotionProductRelationEntity entity : list) {
            boolean b = this.save(entity);
            if (!b){
                return RS.error();
            }
        }
        return RS.ok();
    }

    /*
     *根据id更改
     * */
    @Override
    public RS updateByIdE(@RequestBody FlashPromotionProductRelationEntity entity) {
        boolean b = this.updateById(entity);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    /*
     *根据id集合删除
     * */
    @Override
    @Transactional
    public RS removeByIdsE(@RequestParam("ids") List<Long> ids) {
        boolean b = this.removeByIds(ids);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    /*
     *根据id删除
     * */
    public RS removeByIdE(@RequestParam("id") Long id){
        boolean b = this.removeById(id);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    @Override
    public long getCount(@RequestParam("flashPromotionId") Long flashPromotionId,@RequestParam("flashPromotionSessionId")Long flashPromotionSessionId) {
        int count = this.count(new QueryWrapper<FlashPromotionProductRelationEntity>().eq("flash_promotion_id", flashPromotionId)
                .eq("flash_promotion_session_id", flashPromotionSessionId));
        return count;
    }

}