package cn.mabach.business.feign;

import cn.mabach.goods.service.ProductService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "app-mabach-goods")
public interface ProductServiceFeign extends ProductService {
}
